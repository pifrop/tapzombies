FEATURES

• 100+ Prefab variations (see preview)
• Low Poly Models (enable preview info for tri count)
• 1024 x 1024px Texture & Normal +toon texture
• Includes Scene and preview GUI script
• Easy Drag and Drop Prefabs

Big collections of weapons, shields and a few dungeon props. Low poly meshes that are easily customizable and a single texture that makes combining meshes a breeze.

*Combine meshes requires Simple Mesh Combine

Unluck Software
http://www.chemicalbliss.com/

Thanks for purchasing this asset
Have fun with Unity!
