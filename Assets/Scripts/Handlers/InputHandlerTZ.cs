﻿using UnityEngine;
using System.Collections;

public class InputHandlerTZ : MonoBehaviour
{
	
    /* It handles all imput touches and mouse handler
     *
     * 
     */
    public enum InputType
    {
        Tap,
        DoubleTap,
        Slash,
        LongTouch,
        None
    }

    // Input vars
    private bool newTouch, longPressDetected;
    private float touchTime;
    private float tapTime = -1f;
    private float lastTapTime = -1f;
    private float doubleTapThreshold = 0.5f;
    private bool doubleTap = false;
    private MousePosition prevPos = new MousePosition (Vector3.zero, -1.0f);
    private float strikeDistance;
    private Vector2 dirVec;
    private InputType inputType;
	
    private bool inSecondaryAttack; // 2nd attack mode is when you press for sometime, blade props will enhance.
    private float longPressDetectTime = 0.3f; 
	
    public TrailRenderer blade = null;
    public GameObject hitEffect;
    public GamePlayManagerTZ gamePlayManager;
	
    struct MousePosition
    {
        public Vector3 m_WorldPosition;
        public float m_Time;
        public MousePosition (Vector3 pos, float time)
        {
            m_WorldPosition = pos;
            m_Time = time;
        }
    }
	
    private string dbgLog;
	
    // Use this for initialization
    void Start ()
    {   
        Vector3 pos = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width * .9f, Screen.height * .5f, 10));
        pos.z = 0;
    }
	
    void Update ()
    {
//        TouchHandler ();
        MouseHandler ();
    }
	
    void TouchHandler ()
    {    
        if (Input.touchCount > 0) {
//            double taps = (double)Input.touchCount;
//            GameManagerTZ.UpdateTaps (taps);
            Touch touch = Input.GetTouch (0);
            Vector3 samPos = Camera.main.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, 10));
            samPos.z = 0;
            Ray ray = Camera.main.ScreenPointToRay (touch.position);
//            RaycastHit2D hit;
            RaycastHit hit3D = new RaycastHit ();
//            hit = Physics2D.Raycast (samPos, Vector2.zero);
            Transform objecthit = null;
//            if (GameManagerTZ.isTap3D) {
            if (Physics.Raycast (ray, out hit3D, 100))
                objecthit = hit3D.transform;
//            } else {
//                objecthit = hit.transform;
//            }

            MousePosition presPos = new MousePosition (samPos, Time.time);
			
            Vector3 deltaPos = presPos.m_WorldPosition - prevPos.m_WorldPosition;
            deltaPos.Normalize ();
            dirVec = new Vector2 (deltaPos.x, deltaPos.y);
			
            longPressDetected = false;
            doubleTap = false;
            newTouch = false;
            strikeDistance = 0.0f;
            inputType = InputType.None;
			
            if (touch.phase == TouchPhase.Began) {
//                if (blade) {
//                    blade.transform.position = samPos;   // Setting trail renderer
//                }
                if (inSecondaryAttack) {
                    inSecondaryAttack = false;
                }
				
//                if ((presPos.m_Time - prevPos.m_Time <= doubleTapThreshold) && 
//                    (deltaPos.magnitude < 0.5)) {
//                    doubleTap = true;
//                } else {
                newTouch = true;
//                }
            } else if (touch.phase == TouchPhase.Moved) {
                strikeDistance += Time.deltaTime * touch.deltaPosition.magnitude;
//                if (blade) {
//                    blade.transform.position = presPos.m_WorldPosition;
//                }
            } else if (touch.phase == TouchPhase.Stationary) {
                if (Time.time - presPos.m_Time > longPressDetectTime) {
                    longPressDetected = true;
                }
            } else if ((touch.phase == TouchPhase.Ended) || (touch.phase == TouchPhase.Canceled)) {
                inputType = InputType.None;
            }
			
            if (longPressDetected) {
                inputType = InputType.LongTouch;
            } else if (doubleTap && (touch.tapCount == 2)) {
                inputType = InputType.DoubleTap;
            } else if (newTouch) {
                inputType = InputType.Tap;
            } else if (strikeDistance > 0.1f) {
                inputType = InputType.Slash;
            } else {
                inputType = InputType.None;
            }
			
            if ((hit3D.collider != null)) {
                switch (objecthit.gameObject.tag) {
                case "Enemy":

                    gamePlayManager.EnemyInput (inputType, objecthit.transform.parent.name, 
					                            dirVec, strikeDistance, presPos.m_WorldPosition);
                    break;
                case "GoldCoin":
                    gamePlayManager.GoldCoinInput (inputType, objecthit.gameObject.name);
                    break;
                case "Chest":
                    gamePlayManager.ChestInput (inputType, objecthit.gameObject.name);
                    break;
                case "Loot":
                    break;
                case "NONE":
                    break;
                }
            } else {
                gamePlayManager.NoColliderInput (inputType);
            }
            prevPos = presPos;
        }
    }
	
    private void MouseHandler ()
    {
        if (Input.GetMouseButton (0)) {
            Vector3 sampPos = Vector3.zero;
            Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

            sampPos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0));
            sampPos.z = 0;
            Vector3 tempPos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
            MousePosition presPos = new MousePosition (sampPos, Time.time);
            if (blade) {
                blade.transform.position = sampPos;
            }
            RaycastHit2D hit;
            RaycastHit hit3D = new RaycastHit ();
            
            hit = Physics2D.Raycast (sampPos, Vector2.zero);
            Transform objecthit = null;
            if (GameManagerTZ.isTap3D) {
                if (Physics.Raycast (ray, out hit3D, 100))
                    objecthit = hit3D.transform;
            } else {
                objecthit = hit.transform;
            }
			
            Vector3 deltaPos = presPos.m_WorldPosition - prevPos.m_WorldPosition;
            deltaPos.Normalize ();
            dirVec = new Vector2 (deltaPos.x, deltaPos.y);
            if ((hit.collider != null) || (hit3D.collider != null)) {
                switch (objecthit.gameObject.tag) {
                case "Enemy":
//                    gamePlayManager.EnemyInput (InputType.Slash, objecthit.gameObject.name, dirVec, 0.0f, prevPos.m_WorldPosition);
//                    gamePlayManager.EnemyInput (InputType.Tap, objecthit.transform.parent.name, dirVec, 0.0f, presPos.m_WorldPosition);
                    gamePlayManager.EnemyInput (InputType.Tap, objecthit.transform.parent.name, dirVec, 0.0f, sampPos);
                    //                    EnemyProperties AIscript = objecthit.gameObject.GetComponent<EnemyProperties> ();
					//                    AIscript.Hit (50, hitForce, prevPosition.m_WorldPosition, prevPosition.m_WorldPosition + (new Vector3 (hitForce.x * 4, hitForce.y * 4, 0.0f)));
                    break;
                case "GoldCoin":
                    gamePlayManager.GoldCoinInput (InputType.Tap, objecthit.gameObject.name);
                    break;
                case "Chest":
                    gamePlayManager.ChestInput (inputType, objecthit.gameObject.name);
                    break;
                default:
                    break;
                }
            }
            prevPos = presPos;
        }
    }
}
