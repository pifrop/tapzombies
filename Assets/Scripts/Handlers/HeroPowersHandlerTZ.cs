﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroPowersHandlerTZ : MonoBehaviour
{
    public enum SkillType
    {
        ChargedStrike,
        ArrowStrike,
        CritChance,
        AttackRate,
        TapDamage,
        GoldDrop
    }

    public Animator animator;
//	public bool oneHandSwordIdle = false;
//	public bool idleStandbool = true;
    public GameObject sword;

    public GameObject[] weapons;

    public GameObject[] chargeupEffects;
    public GameObject fireAreaEffect;
    public GameObject windAreaEffect;
    public GameObject[] auraEffects;
    public GameObject darkAuraEffect;
    public GameObject darkHit;
    public GameManagerTZ gameManager;
//    private GameObject currChargeEffect;
    private GameObject currFireObj;
    private GameObject currWindObj;
    private GameObject currAura;
    private GameObject currDarkAura;

    private double heroDamage;
    public static int weaponPos;
    private string prevWeaponName = string.Empty;

    private float attackRate = 1.4f; 
    public static double heroCurrDmg = 0;
    public double heroCurrDps = 0;

    /*******************************************/
    public Vector2 scrollPosition = Vector2.zero;
    public Vector2 scrollPos2 = Vector2.zero;
    public bool idleStandbool = true;
    public bool idleReadybool = false;
    public bool idleMonsterbool = false;
    public bool idleFightbool = false;
    public bool weaponStandbool = false;
    public bool pistolReadybool = false;
    public bool weaponRun = false;
    public bool oneHandSwordIdle = false;
    public bool bowIdle = false;
    public bool motorbikeIdle = false;
    public bool rollerBlade = false;
    public bool skateboard = false;
    public bool climbing = false;
    public bool office = false;
    public bool swim = false;
    public bool wand = false;
    public bool cards = false;
    public bool breakdance = false;
    public bool katana = false;
    public bool soccer = false;
    public bool giant = false;
    public bool zombie = false;
    public bool iceHockey = false;
    public float LHandWeight = 0;
    public Transform LHandPos1;
    public Transform LHandPos2;
    public Transform LHandPos3;
    /***********************************************/

    void Awake ()
    {
//        CheckActiveSkills ();
    }

    public void InitializeWeapons ()
    {
        for (int pos = 0; pos < weapons.Length; pos++) {
            WeaponsData.AddWeapons (weapons [pos].name, pos);
        }
    }

    public class ActiveSkill
    {
        public double multiplier;
        public float timer;

        public ActiveSkill (double multi, float activeTime)
        {
            multiplier = multi;
            timer = activeTime;
        }
    }

    public static Dictionary<SkillType, ActiveSkill> skills = new Dictionary<SkillType, ActiveSkill> (); 

    void Start ()
    {
//		heroPowers.Add (new HeroPower ("BaseDamage", 1, 5, 1));
//		heroPowers.Add (new HeroPower ("ChargedStrike", 1, 5, 1));
//		heroPowers.Add (new HeroPower ("ArrowsAttack", 1, 5, 1));
//		heroPowers.Add (new HeroPower ("CritChance", 1, 5, 1));
//		heroPowers.Add (new HeroPower ("AttackRate", 1, 5, 1));
//		heroPowers.Add (new HeroPower ("TapDamage", 1, 5, 1));
//		heroPowers.Add (new HeroPower ("GoldDrop", 1, 5, 1));

//        animator.gameObject.SetActive (false);
        IdleMeditate ();
//		EnemyPropsTZ aiScript = PoolManager.Props [currentEnemy];a
    }

    void CheckActiveSkills ()
    {
        // This function is called by update which is below.
        if (skills.Count > 0) {
            if (skills.ContainsKey (HeroPowersHandlerTZ.SkillType.ChargedStrike)) {
                CheckActiveSkillTimer (HeroPowersHandlerTZ.SkillType.ChargedStrike, skills [HeroPowersHandlerTZ.SkillType.ChargedStrike].timer);
            }
            if (skills.ContainsKey (HeroPowersHandlerTZ.SkillType.ArrowStrike)) {
                CheckActiveSkillTimer (HeroPowersHandlerTZ.SkillType.ArrowStrike, skills [HeroPowersHandlerTZ.SkillType.ArrowStrike].timer);
            }
            if (skills.ContainsKey (HeroPowersHandlerTZ.SkillType.CritChance)) {
                CheckActiveSkillTimer (HeroPowersHandlerTZ.SkillType.CritChance, skills [HeroPowersHandlerTZ.SkillType.CritChance].timer);
            }
            if (skills.ContainsKey (HeroPowersHandlerTZ.SkillType.AttackRate)) {
                CheckActiveSkillTimer (HeroPowersHandlerTZ.SkillType.AttackRate, skills [HeroPowersHandlerTZ.SkillType.AttackRate].timer);
            }
            if (skills.ContainsKey (HeroPowersHandlerTZ.SkillType.TapDamage)) {
                CheckActiveSkillTimer (HeroPowersHandlerTZ.SkillType.TapDamage, skills [HeroPowersHandlerTZ.SkillType.TapDamage].timer);
            }
            if (skills.ContainsKey (HeroPowersHandlerTZ.SkillType.GoldDrop)) {
                CheckActiveSkillTimer (HeroPowersHandlerTZ.SkillType.GoldDrop, skills [HeroPowersHandlerTZ.SkillType.GoldDrop].timer);
            }
        }
    }


    private void CheckActiveSkillTimer (HeroPowersHandlerTZ.SkillType type, float timerValue)
    {
        if ((Time.time - timerValue) > 0) {
            ChangeBackToNormal (type);
        } else {
            
        }
    }

    public void WeaponUpdated (int pos)
    {
        if (weaponPos == pos) {
            WeaponsData.Weapon currWeapon = WeaponsData.weaponData [pos];
            heroDamage = currWeapon.m_WeaponDps;
            heroCurrDps = heroDamage / (double)attackRate;
            heroCurrDmg = currWeapon.m_Dps;
        }
    }

    public void EquipWeapon (int pos)
    {
        WeaponsData.Weapon currWeapon = WeaponsData.weaponData [pos];
        string name = prevWeaponName;
        if (name.Length == 0) {
            name = "default";
            StartAttack ();
        }
        Transform prevWeapon = animator.gameObject.transform.Find ("Root/Spine/Spine1/Spine2/RightShoulder/RightArm/RightForeArm/RightHand/" + name);
        GameObject instaSword = (GameObject)Instantiate (weapons [pos], prevWeapon.position, prevWeapon.rotation);
        instaSword.transform.SetParent (animator.gameObject.transform.Find ("Root/Spine/Spine1/Spine2/RightShoulder/RightArm/RightForeArm/RightHand"));
        Destroy (prevWeapon.gameObject);
        prevWeaponName = instaSword.name;
        weaponPos = pos;
        heroDamage = currWeapon.m_WeaponDps;
        heroCurrDps = heroDamage / (double)attackRate;
        heroCurrDmg = currWeapon.m_Dps;
    }

    void BowReady ()
    {
        BowIdle ();
        animator.SetBool ("BowReady", true);
    }

    void BowIdle ()
    {
        Falses ();
        bowIdle = true;
        animator.SetBool ("BowIdle", true);
    }

    public void StartAttack ()
    {
        animator.transform.parent.RotateAround (animator.transform.parent.position, Vector3.up, -90);
        OneHandSwordReady ();
        InvokeRepeating ("AttackAnimation", 4.7f, attackRate);
    }

    private void AttackAnimation ()
    {
        if (attackRate < 1.4f) {
            OneHandSwordJabCombo ();
        } else {
            if (Random.Range (0, 100) < 50) {
                OneHandSwordSwing ();
            } else {
                OneHandSwordBackSwing ();
            }
        }
        Invoke ("HitEnemy", 0.6f);
    }

    private void HitEnemy ()
    {
        EnemyPropsTZ aiScript = PoolManager.Props [GameManagerTZ.currentEnemyName];
        aiScript.sideHeroHit (heroDamage);
    }

    public void OnSkillPress (int pos)
    {
        switch (pos) {
        case 1:
            ChargeStrike (pos);
            break;
        case 2:
            FlameStrike (pos);
            break;
        case 3:
            WindStrike (pos);
            break;
        case 4:
            FuryMode (pos);
            break;
        case 5:
            DarkAttack (pos);
            break;
        }
    }

    public void ChangeBackToNormal (SkillType type)
    {
        switch (type) {
        case SkillType.ArrowStrike:
            CancelInvoke ("BurnAttack");
            Destroy (currFireObj);
            break;
        case SkillType.CritChance:
            PlayerData.criticalChance = PlayerData.criticalChance * (1 / (1 + ((float)skills [type].multiplier * 0.01f)));
            Destroy (currWindObj);
            break;
        case SkillType.AttackRate:
            attackRate = attackRate * (1 + ((float)skills [type].multiplier * 0.01f));
            ChangeAttackRate ();
            Destroy (currAura);
            break;
        case SkillType.TapDamage:
            PlayerData.basicTapDamage = PlayerData.basicTapDamage * (1 / (1 + ((float)skills [type].multiplier * 0.01f)));
            Destroy (currDarkAura); 
            break;
        }
        skills.Remove (type);
    }

    private void ChargeStrike (int pos)
    {
        // Add Charge to gameobject. or instantiate charge at this position manually.
        // Deal damage to current Enemy. With invoke after animation length.
//        Instantiate (auraEffects [1], animator.transform.position, auraEffects [1].transform.localRotation);
        Invoke ("HitChargeAttack", 1.5f);
        OneHandHeavyOverhead ();
    }

    private void HitChargeAttack ()
    {
        EnemyPropsTZ aiScript = PoolManager.Props [GameManagerTZ.currentEnemyName];
        aiScript.sideHeroHit (heroDamage * HeroPowersData.heroPowersData [1].m_Increment);
    }

    private void FlameStrike (int pos)
    {
        EnemyPropsTZ aiScript = PoolManager.Props [GameManagerTZ.currentEnemyName];
        Vector3 spawnPos = aiScript.StatusEffect ();
        currFireObj = (GameObject)Instantiate (fireAreaEffect, spawnPos, fireAreaEffect.transform.localRotation);
        double burnRate = HeroPowersData.heroPowersData [pos].m_Increment;
        float activeTime = Time.time + HeroPowersData.heroPowersData [pos].m_Timer;
        skills.Add (SkillType.ArrowStrike, new ActiveSkill (burnRate, activeTime));
        InvokeRepeating ("BurnAttack", 0.1f, (float)burnRate);
    }

    private void BurnAttack ()
    {
        EnemyPropsTZ aiScript = PoolManager.Props [GameManagerTZ.currentEnemyName];
        aiScript.Hit (PlayerData.GetFinalTapDamage (), Vector2.zero, Vector3.zero, Vector3.zero);
    }

    private void WindStrike (int pos)
    {
        EnemyPropsTZ aiScript = PoolManager.Props [GameManagerTZ.currentEnemyName];
        Vector3 spawnPos = aiScript.StatusEffect ();
        currWindObj = (GameObject)Instantiate (windAreaEffect, spawnPos, windAreaEffect.transform.localRotation);
        double critIncr = HeroPowersData.heroPowersData [pos].m_Increment;
        float activeTime = Time.time + HeroPowersData.heroPowersData [pos].m_Timer;
        skills.Add (SkillType.CritChance, new ActiveSkill (critIncr, activeTime));
        PlayerData.criticalChance += PlayerData.criticalChance * (float)critIncr * 0.01f;
    }

    private void FuryMode (int pos)
    {
        currAura = (GameObject)Instantiate (auraEffects [1], animator.transform.position, auraEffects [1].transform.localRotation);
        double attackRateIncr = HeroPowersData.heroPowersData [pos].m_Increment;
        float activeTime = Time.time + HeroPowersData.heroPowersData [pos].m_Timer;
        skills.Add (SkillType.AttackRate, new ActiveSkill (attackRateIncr, activeTime));
        attackRate = attackRate / (1 + ((float)attackRateIncr * 0.01f));
        ChangeAttackRate ();
    }

    private void DarkAttack (int pos)
    {
        EnemyPropsTZ aiScript = PoolManager.Props [GameManagerTZ.currentEnemyName];
        Vector3 spawnPos = aiScript.StatusEffect ();
        currDarkAura = (GameObject)Instantiate (darkAuraEffect, spawnPos, darkAuraEffect.transform.localRotation);
        double tapDmgIncr = HeroPowersData.heroPowersData [pos].m_Increment;
        float activeTime = Time.time + HeroPowersData.heroPowersData [pos].m_Timer;
        skills.Add (SkillType.TapDamage, new ActiveSkill (tapDmgIncr, activeTime));
        PlayerData.basicTapDamage += PlayerData.basicTapDamage * tapDmgIncr * 0.01f;
    }

    private void ChangeAttackRate ()
    {
        if (IsInvoking ("AttackAnimation"))
            CancelInvoke ("AttackAnimation");
        InvokeRepeating ("AttackAnimation", 0, attackRate);
    }

    /// <summary>
    /// Below alla re related to animations
    /// </summary>

    void OneHSwordStrafeLeft ()
    {
        OneHandSwordReady ();
        animator.SetBool ("1HandSwordStrafeLeft", true);
    }
	
    void OneHSwordStrafeRight ()
    {
        OneHandSwordReady ();
        animator.SetBool ("1HandSwordStrafeRight", true);
    }

    void OneHandHeavySwing ()
    {
        OneHandSwordReady ();
        animator.SetTrigger ("1HandHeavySwing");
    }
	
    void IdleMeditate ()
    {
        IdleStand ();
        animator.SetBool ("IdleMeditate", true);
    }

    void IdleStand ()
    {
        Falses ();
        idleStandbool = true;
        animator.SetBool ("IdleStand", true);
    }

    public void OneHandHeavySwing2 ()
    {
        OneHandSwordReady ();
        animator.SetTrigger ("1HandHeavySwing2");
    }
	
    public void OneHandHeavyOverhead ()
    {
        OneHandSwordReady ();
        animator.SetTrigger ("1HandHeavyOverhead");
    }
	
    public void OneHandSmallWeaponCombo ()
    {
        OneHandSwordReady ();
        animator.SetTrigger ("1HandSmallWeaponCombo");
    }
	
    public void OneHandSwordJabCombo ()
    {
        OneHandSwordReady ();
        animator.SetTrigger ("1HandSwordJabCombo");
    }
	
    public void OneHandSwordJabFootPush ()
    {
        OneHandSwordReady ();
        animator.SetTrigger ("1HandSwordJabFootPush");
    }

    public void OneHandSwordRollAttack ()
    {
        OneHandSwordReady ();
        animator.SetBool ("1HandSwordRollAttack", true);
    }

    public void OneHandSwordChargeSwipe ()
    {
        OneHandSwordChargeUp ();
        animator.SetBool ("1HandSwordChargeSwipe", true);
    }
	
    public void OneHandSwordChargeHeavyBash ()
    {
        OneHandSwordChargeUp ();
        animator.SetBool ("1HandSwordChargeHeavyBash", true);
    }
	
    public void OneHandSwordChargeUp ()
    {
        OneHandSwordReady ();
        animator.SetBool ("1HandSwordChargeUp", true);
    }

    public void OneHandSwordShieldBash ()
    {
        OneHandSwordReady ();
        animator.SetBool ("1HandSwordShieldBash", true);
    }
	
    public void OneHandSwordStrafeLeft ()
    {
        OneHandSwordReady ();
        animator.SetBool ("1HSwordStrafeRunLeft", true);
    }
	
    public void OneHandSwordStrafeRight ()
    {
        OneHandSwordReady ();
        animator.SetBool ("1HSwordStrafeRunRight", true);
    }

    public void OneHandSwordRun ()
    {
        Falses ();
        OneHandSwordReady ();
        animator.SetBool ("OneHandSwordRun", true);
    }
	
//	public void OneHandSwordBlock ()
//	{
//		OneHandSwordReady ();
//		animator.SetBool ("OneHandSwordBlock", true);
//	}
	
    public void OneHandSwordJab ()
    {
        OneHandSwordReady ();
        animator.SetBool ("OneHandSwordJab", true);
    }
	
    public void OneHandSwordBackSwing ()
    {
        OneHandSwordReady ();
        animator.SetBool ("OneHandSwordBackSwing", true);
    }
	
    public void OneHandSwordSwing ()
    {
        OneHandSwordReady ();
        animator.SetBool ("OneHandSwordSwing", true);
    }
	
    public void OneHandSwordReady ()
    {
        OneHandSwordIdle ();
        animator.SetBool ("OneHandSwordReady", true);
    }
	
    public void OneHandSwordIdle ()
    {
        Falses ();
        oneHandSwordIdle = true;
        animator.SetBool ("OneHandSwordIdle", true);
    }

    void Falses ()
    {
        weaponStandbool = false;
        idleStandbool = false;
        idleReadybool = false;
        idleMonsterbool = false;
        idleFightbool = false;
        weaponRun = false;
        oneHandSwordIdle = false;
        pistolReadybool = false;
        bowIdle = false;
        motorbikeIdle = false;
        rollerBlade = false;
        skateboard = false;
        climbing = false;
        office = false;
        swim = false;
        wand = false;
        cards = false;
        breakdance = false;
        katana = false;
        soccer = false;
        giant = false;
        zombie = false;
        iceHockey = false;
		
//		animator.SetBool ("IceHockeyGoalieReady", false);
//		animator.SetBool ("IceHockeyDekeMiddle", false);
//		animator.SetBool ("IceHockeyIdle", false);

//		animator.SetBool ("1HandSwordStrafeLeft", false);
//		animator.SetBool ("1HandSwordStrafeRight", false);

//		animator.SetBool ("ZombieCrawl", false);
//		animator.SetBool ("ZombieWalk", false);
//		animator.SetBool ("ZombieIdle", false);
//		animator.SetBool ("WoodSaw", false);
//		animator.SetBool ("BlackSmithHammer", false);
//		animator.SetBool ("GiantGrabIdle2", false);
//		animator.SetBool ("GiantGrabIdle", false);
//		animator.SetBool ("WallSit", false);
//		animator.SetBool ("WallRunLeft", false);
//		animator.SetBool ("WallRunRight", false);
//		animator.SetBool ("ScubaSwim", false);
//		animator.SetBool ("BackPackOff", false);
//		animator.SetBool ("SneakForward", false);
//		animator.SetBool ("SneakBackward", false);
//		animator.SetBool ("SneakLeft", false);
//		animator.SetBool ("SneakRight", false);
//		animator.SetBool ("SneakIdle", false);
//		animator.SetBool ("SoccerRun", false);
//		animator.SetBool ("SoccerSprint", false);
//		animator.SetBool ("SoccerWalk", false);
//		animator.SetBool ("SoccerKeeperReady", false);

//		animator.SetBool ("Katana", false);
//		animator.SetBool ("KatanaReadyHigh", false);
//		animator.SetBool ("KatanaReady", false);
//		animator.SetBool ("KatanaReadyLow", false);
//		animator.SetBool ("KatanaReady", false);

//		animator.SetBool ("KneesIdle", false);
//		animator.SetBool ("WalkInjured", false);
//		animator.SetBool ("SatNightFever", false);
//		animator.SetBool ("RunningDance", false);
//		animator.SetBool ("RussianDance", false);
//		animator.SetBool ("ElvisLegsLoop", false);
//		animator.SetBool ("Flashlight", false);
//		animator.SetBool ("WalkBackward", false);
//		animator.SetBool ("Windmill", false);
//		animator.SetBool ("Flares", false);
//		animator.SetBool ("DeadmanFloat", false);
//		animator.SetBool ("2000", false);
//		animator.SetBool ("SixStep", false);
//		animator.SetBool ("ChannelCastOmni", false);
//		animator.SetBool ("ChannelCastDirected", false);

//		animator.SetBool ("BowInstant2", false);
//		animator.SetBool ("BowReady2", false);

//		animator.SetBool ("WalkDehydrated", false);
//		animator.SetBool ("UpHillWalk", false);
//		animator.SetBool ("CardPlayerIdle", false);
//		animator.SetBool ("DealerIdle", false);
//		animator.SetBool ("StaffStand", false);
//		animator.SetBool ("WandStand", false);
//		animator.SetBool ("SwimDogPaddle", false);
//		animator.SetBool ("SwimFreestyle", false);
//		animator.SetBool ("Swim", false);
//		animator.SetBool ("WateringCan", false);
//		animator.SetBool ("OfficeSittingReadingLeanBack", false);
//		animator.SetBool ("OfficeSittingReading", false);
//		animator.SetBool ("OfficeSittingLegCross", false);
//		animator.SetBool ("OfficeSittingBack", false);
//		animator.SetBool ("OfficeSitting45DegLeg", false);
//		animator.SetBool ("OfficeSitting1LegStraight", false);
//		animator.SetBool ("OfficeSitting", false);
//		animator.SetBool ("ClimbUp", false);
//		animator.SetBool ("ClimbIdle", false);
//		animator.SetBool ("SkateboardIdle", false);
//		animator.SetBool ("IdleFeedThrow", false);
//		animator.SetBool ("RollerBladeCrossoverLeft", false);
//		animator.SetBool ("RollerBladeTurnLeft", false);
//		animator.SetBool ("RollerBladeTurnRight", false);
//		animator.SetBool ("RollerBladeSkateFwd", false);
//		animator.SetBool ("RollerBladeCrossoverRight", false);
//		animator.SetBool ("RollerBladeGrindRoyale", false);
//		animator.SetBool ("RollerBladeRoll", false);
//		animator.SetBool ("RollerBladeStand", false);
//		animator.SetBool ("CrouchStrafeLeft", false);
//		animator.SetBool ("CrouchStrafeRight", false);
//		animator.SetBool ("CrouchWalkBackward", false);
//		animator.SetBool ("ProneLocomotion", false);
//		animator.SetBool ("ProneIdle", false);
//		animator.SetBool ("CrawlLocomotion", false);
//		animator.SetBool ("CrawlIdle", false);
//		animator.SetBool ("RunBackRight", false);
//		animator.SetBool ("RunBackLeft", false);
//		animator.SetBool ("WorkerShovel2", false);
//		animator.SetBool ("WorkerShovel", false);
//		animator.SetBool ("WorkerPickaxe", false);
//		animator.SetBool ("WorkerPickaxe2", false);
//		animator.SetBool ("WorkerHammer2", false);
//		animator.SetBool ("WorkerHammer", false);
//		animator.SetBool ("WoodCut", false);
//		animator.SetBool ("MotorbikeLasso", false);
//		animator.SetBool ("MotorbikeWheelyNoHands", false);
//		animator.SetBool ("MotorbikeWheely", false);
//		animator.SetBool ("MotorbikeSeatStandWheely", false);
//		animator.SetBool ("MotorbikeSeatStand", false);
//		animator.SetBool ("MotorbikeLookBack", false);
//		animator.SetBool ("MotorbikeHeartAttack", false);
//		animator.SetBool ("MotorbikeHeadstand", false);
//		animator.SetBool ("MotorbikeHandstand", false);
//		animator.SetBool ("MotorbikeHandlebarSit", false);
//		animator.SetBool ("MotorbikeBackwardStand", false);
//		animator.SetBool ("MotorbikeBackwardSitting", false);
//		animator.SetBool ("MotorbikeAirWalk", false);
//		animator.SetBool ("MotorbikeIdle", false);
//		animator.SetBool ("IdleStun", false);
        animator.SetBool ("1HandSwordChargeUp", false);
//		animator.SetBool ("WeaponRunBackward", false);
//		animator.SetBool ("RunBackward", false);
        animator.SetBool ("1HSwordStrafeRunRight", false);
        animator.SetBool ("1HSwordStrafeRunLeft", false);
//		animator.SetBool ("WeaponStrafeRunRight", false);
//		animator.SetBool ("WeaponStrafeRunLeft", false);
//		animator.SetBool ("StrafeRunRight", false);
//		animator.SetBool ("StrafeRunLeft", false);
//		animator.SetBool ("FlyUp", false);
//		animator.SetBool ("FlyDown", false);
//		animator.SetBool ("FlyRight", false);
//		animator.SetBool ("FlyLeft", false);
//		animator.SetBool ("FlyBackward", false);
//		animator.SetBool ("FlyForward", false);
//		animator.SetBool ("IdleFly", false);
        animator.SetBool ("IdleMeditate", false);
//		animator.SetBool ("ShotgunReloadMagazine", false);
//		animator.SetBool ("BowReady", false);
//		animator.SetBool ("BowInstant", false);
//		animator.SetBool ("BowFire", false);
//		animator.SetBool ("IdleStrafeLeft", false);
//		animator.SetBool ("IdleStrafeRight", false);
//		animator.SetBool ("CratePull", false);
//		animator.SetBool ("CratePush", false);
//		animator.SetBool ("IdleWalk", false);
//		animator.SetBool ("WeaponRun", false);
//		animator.SetBool ("WeaponStand", false);
//		animator.SetBool ("IdleReady", false);
        animator.SetBool ("IdleStand", false);
//		animator.SetBool ("IdleMonster", false);
//		animator.SetBool ("WeaponReady", false);
//		animator.SetBool ("WeaponInstant", false);
//		animator.SetBool ("IdleFight", false);
//		animator.SetBool ("PistolReady", false);
//		animator.SetBool ("PistolInstant", false);
        animator.SetBool ("OneHandSwordIdle", false);
        animator.SetBool ("OneHandSwordReady", false);
        animator.SetBool ("OneHandSwordRun", false);
//		animator.SetBool ("IdleRun", false);
//		animator.SetBool ("BowIdle", false);
//		animator.SetBool ("IdleReadyCrouch", false);
//		animator.SetBool ("CrouchWalk", false);
    }
	
	
    void Update ()
    {
        CheckActiveSkills ();
		
        if (animator.GetFloat ("Curve") > 0.1f) {
//			if (animator.GetBool ("BowFire")) {
//				BowReady ();
//			}
			
			
//			animator.SetBool ("Giant2HandSlamIdle", false);
//			animator.SetBool ("GiantGrabIdle2", false);
//			animator.SetBool ("GiantGrabIdle", false);
//			animator.SetBool ("WateringCanWatering", false);
//			animator.SetBool ("OfficeSittingReadingPageFlip", false);
//			animator.SetBool ("OfficeSittingEyesRub", false);
//			animator.SetBool ("OfficeSittingHandRestFingerTap", false);
//			animator.SetBool ("OfficeSittingMouseMovement", false);
//			animator.SetBool ("OfficeSittingReadingCoffeeSip", false);
//			animator.SetBool ("VaderChoke", false);
//			animator.SetBool ("HeelClick", false);
//			animator.SetBool ("Yawn", false);
//			animator.SetBool ("360SpinDeath", false);
//			animator.SetBool ("ClimbLeft", false);
//			animator.SetBool ("ClimbRight", false);
//			animator.SetBool ("SkateboardKickPush", false);
//			animator.SetBool ("IdleStandingJump", false);
//			animator.SetBool ("IdleSlide", false);
//			animator.SetBool ("RollerBladeFrontFlip", false);
//			animator.SetBool ("RollerBladeBackFlip", false);
//			animator.SetBool ("RollerBladeStop", false);
//			animator.SetBool ("RollerBladeJump", false);
//			animator.SetBool ("MotorbikeSuperman", false);
//			animator.SetBool ("MotorbikeSpecialFlip", false);
//			animator.SetBool ("PistolLeftHandStab", false);
//			animator.SetBool ("IdleMouthWipe", false);
//			animator.SetBool ("IdleSpew", false);
            animator.SetBool ("1HandSwordRollAttack", false);
//			animator.SetBool ("MotorbikeTurnRight", false);
//			animator.SetBool ("MotorbikeTurnLeft", false);
//			animator.SetBool ("MotorbikeShootLeft", false);
//			animator.SetBool ("MotorbikeShootRight", false);
//			animator.SetBool ("MotorbikeShootFwd", false);
//			animator.SetBool ("MotorbikeShootBack", false);
//			animator.SetBool ("MotorbikeLassoRight", false);
//			animator.SetBool ("MotorbikeLassoLeft", false);
//			animator.SetBool ("MotorbikeLassoBack", false);
//			animator.SetBool ("MotorbikeLassoFwd", false);
//			animator.SetBool ("MotorbikeBackwardSittingCheer", false);
//			animator.SetBool ("WeaponReadyFire", false);
//			animator.SetBool ("ShotgunReadyFire", false);
            animator.SetBool ("1HandSwordChargeUp", false);
            animator.SetBool ("1HandSwordChargeSwipe", false);
            animator.SetBool ("1HandSwordChargeHeavyBash", false);
//			animator.SetBool ("1HandSwordShieldBash", false);
//			animator.SetBool ("Crouch180", false);
//			animator.SetBool ("WizardNeoBlock", false);
//			animator.SetBool ("WizardEyeBeam", false);
//			animator.SetBool ("ShotgunFire", false);
//			animator.SetBool ("IdleDodgeLeft", false);
//			animator.SetBool ("IdleDodgeRight", false);
//			animator.SetBool ("RunDive", false);
//			animator.SetBool ("RunJump", false);
//			animator.SetBool ("Cowboy1HandDraw", false);
//			animator.SetBool ("WeaponReload", false);
//			animator.SetBool ("WeaponFire", false);
//			animator.SetBool ("RHandPunch", false);
//			animator.SetBool ("LHandPunch", false);
//			animator.SetBool ("FaceHit", false);
//			animator.SetBool ("FrontKick", false);
//			animator.SetBool ("IdleCheer", false);
//			animator.SetBool ("ComeHere", false);
//			animator.SetBool ("IdleKeepBack", false);
//			animator.SetBool ("IdleReadyLook", false);
//			animator.SetBool ("IdleSad", false);
//			animator.SetBool ("Wizard1HandThrow", false);
//			animator.SetBool ("Wizard2HandThrow", false);
//			animator.SetBool ("WizardBlock", false);
//			animator.SetBool ("WizardOverhead", false);
//			animator.SetBool ("WizardPowerUp", false);
//			animator.SetBool ("PistolFire", false);
//			animator.SetBool ("PistolReload", false);
            animator.SetBool ("OneHandSwordSwing", false);
            animator.SetBool ("OneHandSwordBackSwing", false);
            animator.SetBool ("OneHandSwordJab", false);
//			animator.SetBool ("OneHandSwordBlock", false);
//			animator.SetBool ("IdleDie", false);
//			animator.SetBool ("IdleTurns", false);
//			animator.SetBool ("ShotgunReloadChamber", false);
//			animator.SetBool ("NadeThrow", false);
//			animator.SetBool ("Idle180", false);
//			animator.SetBool ("IdleButtonPress", false);
//			animator.SetBool ("IdleTyping", false);
			
        }
		
    }
}
