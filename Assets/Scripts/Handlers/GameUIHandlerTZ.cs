﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameUIHandlerTZ : MonoBehaviour
{
    public GameUIManagerTZ gameUIManager;
//	public static AchievementsData.Achievement tempAchievement;
    public static List<WeaponsData.WeaponType> weaponTypeUnlockStatus = new List<WeaponsData.WeaponType> ();

    public void CheckWeaponButtonStatus ()
    {
        foreach (WeaponsData.Weapon weapon in WeaponsData.weaponData) {
            if (GameManagerTZ.totalGold >= weapon.m_Cost) {
                if (weapon.m_Level == 0) {
                    //add new Label to button and if there no new on bar add it. and add +1 to new heros
                } else {
                    AddTypeToDic (weapon.m_Type);
                }
                //Change color to enable button
                gameUIManager.UpdateWeaponButton (weapon.m_Name, weapon.m_Cost, weapon.m_Dps, weapon.m_Level, true);
            } else {
                gameUIManager.UpdateWeaponButton (weapon.m_Name, weapon.m_Cost, weapon.m_Dps, weapon.m_Level, false);
            }
        }
    }

    public void CheckHeroPowersStatus ()
    {
        if (weaponTypeUnlockStatus.Count == 0) {
            foreach (HeroPowersData.HeroPower heroPower in HeroPowersData.heroPowersData) {
                if (heroPower.m_Name.Equals ("heroBaseDmgUpgrade")) {
                    continue;
                }
                double incr = heroPower.m_Increment;
                if (heroPower.m_Level == 0) {
                    incr = heroPower.m_Increment;
                } else {
                    incr = heroPower.m_BaseIncrement;
                }
                gameUIManager.UpdateHeroButton (heroPower.m_Name,
                                                heroPower.m_Cost,
                                                incr,
                                                heroPower.m_Level,
                                                false);
            }
        } else {
            foreach (WeaponsData.WeaponType type in weaponTypeUnlockStatus) {
                int upgradePos = 0;
                switch (type) {
                case WeaponsData.WeaponType.Axe:
                    upgradePos = 1;
                    break;
                case WeaponsData.WeaponType.Sword:
                    upgradePos = 2;
                    break;
                case WeaponsData.WeaponType.Mace:
                    upgradePos = 3;
                    break;
                case WeaponsData.WeaponType.BattleHammer:
                    upgradePos = 4;
                    break;
                case WeaponsData.WeaponType.Rare:
                    upgradePos = 5;
                    break;
                case WeaponsData.WeaponType.Epic:
                    upgradePos = 6;
                    break;
                }
                double incr = HeroPowersData.heroPowersData [upgradePos].m_Increment;
                if (HeroPowersData.heroPowersData [upgradePos].m_Level == 0) {
                    incr = HeroPowersData.heroPowersData [upgradePos].m_Increment;
                } else {
                    incr = HeroPowersData.heroPowersData [upgradePos].m_BaseIncrement;
                }
                if (GameManagerTZ.totalGold >= HeroPowersData.heroPowersData [upgradePos].m_Cost) {
                    // Enable button
                    gameUIManager.UpdateHeroButton (HeroPowersData.heroPowersData [upgradePos].m_Name,
                                                    HeroPowersData.heroPowersData [upgradePos].m_Cost,
                                                    incr,
                                                    HeroPowersData.heroPowersData [upgradePos].m_Level,
                                                    true);
                } else {
                    // disable button
                    gameUIManager.UpdateHeroButton (HeroPowersData.heroPowersData [upgradePos].m_Name,
                                                    HeroPowersData.heroPowersData [upgradePos].m_Cost,
                                                    incr,
                                                    HeroPowersData.heroPowersData [upgradePos].m_Level,
                                                    false);
                }
            }
        }
        if (HeroPowersData.heroPowersData [0].m_Cost <= GameManagerTZ.totalGold) {
            gameUIManager.UpdateHeroButton (HeroPowersData.heroPowersData [0].m_Name,
                                            HeroPowersData.heroPowersData [0].m_Cost,
                                            HeroPowersData.heroPowersData [0].m_Increment,
                                            HeroPowersData.heroPowersData [0].m_Level,
                                            true);
        } else {
            gameUIManager.UpdateHeroButton (HeroPowersData.heroPowersData [0].m_Name,
                                            HeroPowersData.heroPowersData [0].m_Cost,
                                            HeroPowersData.heroPowersData [0].m_Increment,
                                            HeroPowersData.heroPowersData [0].m_Level,
                                            false);
        }
    }

    public void CheckArtifactsStatus ()
    {

    }

    public void CheckAchSingleStatus (AchievementsData.Achievement tempAchievement)
    {
        int Starsasd = tempAchievement.m_Stars;
        string lvlText = string.Format ("{0}", LevelDataUtil.ToStringFormat (tempAchievement.m_Value)) + "/" + string.Format ("{0}", LevelDataUtil.ToStringFormat (tempAchievement.m_StarConstants [tempAchievement.m_Stars]));
        string lvlname = tempAchievement.m_Name;
        double DiamondNo = tempAchievement.Diamonds [tempAchievement.m_Stars];
        int Stars = tempAchievement.m_Stars;
        gameUIManager.UpdateAchievementText (lvlname, lvlText, true);
        gameUIManager.UpdateStarColor (lvlname, Stars, true);
        if (tempAchievement.isUnlocked ()) {
            gameUIManager.UpdateAchievementButton (lvlname, DiamondNo, true);
        } else {
            gameUIManager.UpdateAchievementButton (lvlname, DiamondNo, false);
        }
    }

    public void UpdateAllAchievementStatus ()
	{
        foreach (var achievement in  AchievementsData.achievementsDictionary) {
            CheckAchSingleStatus (achievement.Value);
        }
    }

    public void AddTypeToDic (WeaponsData.WeaponType type)
    {
        if (!weaponTypeUnlockStatus.Contains (type)) {
            weaponTypeUnlockStatus.Add (type);
        }
    }

}
