﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
//using Soomla.Store;

public class GameUIManagerTZ : MonoBehaviour
{
//    public enum Panel
//    {
//        HeroPowers,
//        Weapons,
//        Artifacts,
//        Revenue
//    }
    public Animator heroPowersPanel;
    public Animator weaponsPanel;
    public Animator artifactsPanel;
    public Animator revenuePanel;
    public Animator achievementsPanel;
    public Animator RectPanel;
    public RectTransform activePowersPanel;
    public RectTransform inactivePowerspanel;
    public RectTransform holderpanel;
    public RectTransform openButtonPanel;
    public RectTransform skillRectPanel;
    public RectTransform weaponActivePanel;
    public RectTransform heroActivePanel;
//	private static Vector2 spawnPos = Vector2.zero;
    public GameObject dmgPopupPrefab;
    public Text enemyHealthDisplayText;
    public Text enemyNameText;
    public Image enemyHealthRenderer;
    public Image bossProgressTimer;
    public Text bossTimerText;
    public Text gold;
    public Text starDust;
    public Text Diamond;
    public Text currDps;
    public Text weaponDps;
    public Text tapDps;
    public Text gameLevelText;
    public Text gameNextLevelText;
    public Image LevelRectRenderer;
    public Text enemyNumberText;
    public Image AchievementPopupImage;
    public Image[] powerImages;
    public Button fightBoss;
    public Button leaveBattle;
    public HeroPowersHandlerTZ heroPowersHandler;
    public GameUIHandlerTZ gameUIHandler;
    public GameManagerTZ gameManager;

    public static GameObject damagePopup;
    private static string healthDisplay;
    private static string enemyName;
    private static string bossTimer;
    private static float healthReduction = 101;
    public double timer;
    public float activeTimer;





    public Dictionary<HeroPowersHandlerTZ.SkillType, float> skillTimers = new Dictionary<HeroPowersHandlerTZ.SkillType, float> ();
    public Dictionary<HeroPowersHandlerTZ.SkillType,float> skillActiveTimers = new Dictionary<HeroPowersHandlerTZ.SkillType,float> ();
    void Awake ()
    {
        damagePopup = dmgPopupPrefab;
        for (int pos=1; pos<7; pos++) {
            HeroPowersHandlerTZ.SkillType type = ConvertPosToSkill (pos);

//            if (HeroPowersData.heroPowersData [pos].m_Cost <= GameManagerTZ.totalGold) {
            if (HeroPowersData.heroPowersData [pos].m_Level >= 1) {
                activePowersPanel.FindChild (HeroPowersData.heroPowersData [pos].m_Name).gameObject.SetActive (true);
                inactivePowerspanel.FindChild (HeroPowersData.heroPowersData [pos].m_Name).gameObject.SetActive (true);
//                    CheckCoolDownTimer (type, skillTimers [type]);
//                }
            }
        }

        //        CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.ChargedStrike, skillTimers [HeroPowersHandlerTZ.SkillType.ChargedStrike]);
//        CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.ArrowStrike, skillTimers [HeroPowersHandlerTZ.SkillType.ArrowStrike]);
//        CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.CritChance, skillTimers [HeroPowersHandlerTZ.SkillType.CritChance]);
//        CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.AttackRate, skillTimers [HeroPowersHandlerTZ.SkillType.AttackRate]);
//        CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.TapDamage, skillTimers [HeroPowersHandlerTZ.SkillType.TapDamage]);
//        CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.GoldDrop, skillTimers [HeroPowersHandlerTZ.SkillType.GoldDrop]);
    }
    
    private void Start ()
    {
        InitiateSoomla ();
//        InitiateUniBill ();
    }
    
    private void Update ()
    {
        enemyHealthDisplayText.text = healthDisplay;
        enemyNameText.text = enemyName;
        if (healthReduction != 101) {
            enemyHealthRenderer.color = Color.Lerp (Color.green, Color.red, 1 - healthReduction * 0.01f);
            enemyHealthRenderer.fillAmount = healthReduction * 0.01f;
        }
        if (string.IsNullOrEmpty (bossTimer)) {
            bossTimerText.enabled = false;
            bossProgressTimer.enabled = false;
        } else {
            bossTimerText.enabled = true;
            bossProgressTimer.enabled = true;
            bossTimerText.text = bossTimer;
            bossProgressTimer.fillAmount = (1.0f / 30.0f) * float.Parse (bossTimer);
        }
        gold.text = LevelDataUtil.ToStringFormat (GameManagerTZ.totalGold);
        currDps.text = string.Format ("{0}", LevelDataUtil.ToStringFormat (GameManagerTZ.currentDps));
        weaponDps.text = string.Format ("{0}", LevelDataUtil.ToStringFormat (heroPowersHandler.heroCurrDps));
        tapDps.text = string.Format ("{0}", LevelDataUtil.ToStringFormat (PlayerData.basicTapDamage));
        starDust.text = LevelDataUtil.ToStringFormat (GameManagerTZ.totalDiamonds);
        Diamond.text = LevelDataUtil.ToStringFormat (GameManagerTZ.totalDiamonds);
        if (skillTimers.Count > 0) {
            if (skillTimers.ContainsKey (HeroPowersHandlerTZ.SkillType.ChargedStrike)) {
                CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.ChargedStrike, skillTimers [HeroPowersHandlerTZ.SkillType.ChargedStrike]);
            }
            if (skillTimers.ContainsKey (HeroPowersHandlerTZ.SkillType.ArrowStrike)) {
                CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.ArrowStrike, skillTimers [HeroPowersHandlerTZ.SkillType.ArrowStrike]);
            }
            if (skillTimers.ContainsKey (HeroPowersHandlerTZ.SkillType.CritChance)) {
                CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.CritChance, skillTimers [HeroPowersHandlerTZ.SkillType.CritChance]);
            }
            if (skillTimers.ContainsKey (HeroPowersHandlerTZ.SkillType.AttackRate)) {
                CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.AttackRate, skillTimers [HeroPowersHandlerTZ.SkillType.AttackRate]);
            }
            if (skillTimers.ContainsKey (HeroPowersHandlerTZ.SkillType.TapDamage)) {
                CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.TapDamage, skillTimers [HeroPowersHandlerTZ.SkillType.TapDamage]);
            }
            if (skillTimers.ContainsKey (HeroPowersHandlerTZ.SkillType.GoldDrop)) {
                CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType.GoldDrop, skillTimers [HeroPowersHandlerTZ.SkillType.GoldDrop]);
            }
        }
    }

    private void CheckCoolDownTimer (HeroPowersHandlerTZ.SkillType type, float timerValue)
    {
        int pos = ConvertSkillToPos (type);
        System.TimeSpan span = System.DateTime.Now.Subtract (new System.DateTime (1970, 1, 1, 0, 0, 0, 0));
        double tempTime = gameManager.elapsedTIme;
//        double tempTime = span.TotalSeconds - activeTimer;
        float temp = (float)tempTime + Time.time;
        if (temp - timerValue >= 0) {
//            if ((Time.time - timerValue) > 0) {
            inactivePowerspanel.FindChild (HeroPowersData.heroPowersData [pos].m_Name).GetComponent <Button> ().enabled = true;
            inactivePowerspanel.FindChild (HeroPowersData.heroPowersData [pos].m_Name).GetComponentInChildren <Text> ().text = "";
            powerImages [pos - 1].fillMethod = Image.FillMethod.Radial360;
            powerImages [pos - 1].fillAmount = 0;
            //Remove the element from timers 
            skillTimers.Remove (type);
            //          heroPowersHandler.ChangeBackToNormal (type);
        } else {
            //Show Timer.
            int mins = (int)(timerValue - temp) / 60;
            int secs = (int)(timerValue - temp) % 60;
            //                int mins = (int)(timerValue - Time.time) / 60;
//                int secs = (int)(timerValue - Time.time) % 60;
            powerImages [pos - 1].fillAmount = (timerValue - temp) / HeroPowersData.heroPowersData [pos].m_Cooldown;
            inactivePowerspanel.FindChild (HeroPowersData.heroPowersData [pos].m_Name).GetComponentInChildren <Text> ().text = mins.ToString ("D2") + ":" + secs.ToString ("D2");
            inactivePowerspanel.FindChild (HeroPowersData.heroPowersData [pos].m_Name).GetComponent <Button> ().enabled = false;   
        }
    }
//}

    public void OnActiveSkill (int pos)
    {
        HeroPowersHandlerTZ.SkillType type = ConvertPosToSkill (pos);
        heroPowersHandler.OnSkillPress (pos);
        // disable untill cool down
        if (pos == 1) {
            gameManager.ChargeUsed ();
        }
        skillTimers.Add (type, (Time.time + HeroPowersData.heroPowersData [pos].m_Cooldown));


    }
	
    public void EmailUs ()
    {
        //email Id to send the mail to
        string email = "pifrop@gmail.com";
        //subject of the mail
        string subject = MyEscapeURL ("FEEDBACK/SUGGESTION");
        //body of the mail which consists of Device Model and its Operating System
        string body = MyEscapeURL ("Please Enter your message here\n\n\n\n" +
            "________" +
            "\n\nPlease Do Not Modify This\n\n" +
            "Model: " + SystemInfo.deviceModel + "\n\n" +
            "OS: " + SystemInfo.operatingSystem + "\n\n" +
            "________");
        //Open the Default Mail App
        Application.OpenURL ("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }  
    
    string MyEscapeURL (string url)
    {
        return WWW.EscapeURL (url).Replace ("+", "%20");
    }
    
    public static PopDamageScript CreatePopupString (Vector3 pos)
    { 
        GameObject damageGameObject = PoolManager.Pools ["ParticleEffects"].Spawn (damagePopup, pos, Quaternion.identity);
        PopDamageScript pScript = damageGameObject.GetComponent<PopDamageScript> ();
        return pScript;
    }

    public void UpdateEnemyNumber (string enemyNum)
    {
        enemyNumberText.text = enemyNum;
    }

    public void UpdateGameLevel (string level, string nextlevel)
    {
        gameLevelText.text = level;
        gameNextLevelText.text = nextlevel;
    }

    public void UpdateBossButtonStatus (bool isFight, bool enable)
    {
        if (enable) {
            if (isFight) {
                fightBoss.image.enabled = enable;
                fightBoss.GetComponentInChildren <Text> ().enabled = enable;
                leaveBattle.image.enabled = !enable;
                leaveBattle.GetComponentInChildren <Text> ().enabled = !enable;
            } else {
                fightBoss.image.enabled = !enable;
                fightBoss.GetComponentInChildren <Text> ().enabled = !enable;
                leaveBattle.image.enabled = enable;
                leaveBattle.GetComponentInChildren <Text> ().enabled = enable;
            }
        } else {
            fightBoss.image.enabled = enable;
            fightBoss.GetComponentInChildren <Text> ().enabled = enable;
            leaveBattle.image.enabled = enable;
            leaveBattle.GetComponentInChildren <Text> ().enabled = enable;
        }

    }

    public static void UpdateHealthDisplay (string health)
    {
        healthDisplay = string.Format ("{0} HP", health);
    }
    public static void UpdateEnemyName (string name)
    {
        enemyName = name;
    }

    public static void UpdateHealthBar (float healthRed)
    {
        healthReduction = healthRed;
    }

    public static void UpdateBossTimer (string timer)
    {
        bossTimer = timer;
    }

    public void OnFightBoss ()
    {
        EnemyPropsTZ aiScript = PoolManager.Props [GameManagerTZ.currentEnemyName];
        aiScript.RemoveEnemy ();
        GameManagerTZ.isFarming = false;
        GameManagerTZ.isEnemyLoaded = false;
    }

    public void OnLeaveBattle ()
    {
        EnemyPropsTZ aiScript = PoolManager.Props [GameManagerTZ.currentEnemyName];
        aiScript.RemoveEnemy ();
        GameManagerTZ.FailedToKillBoss ();
    }
    public static void BossFailMessage ()
    {
        Vector3 VFailBossMessage = new Vector3 (0f, 2f, -0.5f);
        PopDamageScript popupScript = CreatePopupString (VFailBossMessage);
        popupScript.AttachString ("Failed To Kill Boss,Get Ready to Farming");
        popupScript.destructionTime = 8.0f;
        popupScript.risingSpeed = 0.01f;
        popupScript.displayText.color = new Color32 (155, 255, 155, 255);
        popupScript.displayText.fontSize = 14;
    }

    public void OpenPanel (int panelType)
    {   
        Button revenueRectButton = RectPanel.GetComponent<RectTransform> ().FindChild ("revenueButton").GetComponent <Button> ();
        Button heroRectButton = RectPanel.GetComponent<RectTransform> ().FindChild ("heroButton").GetComponent <Button> ();
        Button weaponRectButton = RectPanel.GetComponent<RectTransform> ().FindChild ("weaponButton").GetComponent <Button> ();
        switch (panelType) {
        case 1:
            heroPowersPanel.SetBool ("isHidden", false);
            weaponsPanel.SetBool ("isHidden", true);
            artifactsPanel.SetBool ("isHidden", true);
            revenuePanel.SetBool ("isHidden", true);
            RectPanel.SetBool ("isHidden", false);
            heroRectButton.interactable = false;
            weaponRectButton.interactable = true;
            revenueRectButton.interactable = true;
            break;
        case 2:
            heroPowersPanel.SetBool ("isHidden", true);
            weaponsPanel.SetBool ("isHidden", false);
            artifactsPanel.SetBool ("isHidden", true);
            revenuePanel.SetBool ("isHidden", true);
            RectPanel.SetBool ("isHidden", false);
            heroRectButton.interactable = true;
            weaponRectButton.interactable = false;
            revenueRectButton.interactable = true;
            break;
        case 3:
            heroPowersPanel.SetBool ("isHidden", true);
            weaponsPanel.SetBool ("isHidden", true);
            artifactsPanel.SetBool ("isHidden", false);
            revenuePanel.SetBool ("isHidden", true);
            RectPanel.SetBool ("isHidden", false);
            heroRectButton.interactable = true;
            weaponRectButton.interactable = true;
            revenueRectButton.interactable = true;
            break;
        case 4:
            heroPowersPanel.SetBool ("isHidden", true);
            weaponsPanel.SetBool ("isHidden", true);
            artifactsPanel.SetBool ("isHidden", true);
            revenuePanel.SetBool ("isHidden", false);
            RectPanel.SetBool ("isHidden", false);
            heroRectButton.interactable = true;
            weaponRectButton.interactable = true;
            revenueRectButton.interactable = false;
            break;
        }
    }

    
    public void OpenAchievementsPanel ()
    {
        achievementsPanel.SetBool ("isHidden", false);
    }

    public void CloseAchievementsPanel ()
    {
        achievementsPanel.SetBool ("isHidden", true);
    }

    public void ClosePanel ()
    {
        heroPowersPanel.SetBool ("isHidden", true);
        weaponsPanel.SetBool ("isHidden", true);
        artifactsPanel.SetBool ("isHidden", true);
        revenuePanel.SetBool ("isHidden", true);
        RectPanel.SetBool ("isHidden", true);  
    }
    
    public void OnUpgradeHero (int upgradePos)
    {
        switch (upgradePos) {
        case 0:
            CheckAndUpdateHeroPowerStatus (upgradePos);
            break;
        case 1:
            if (GameUIHandlerTZ.weaponTypeUnlockStatus.Contains (WeaponsData.WeaponType.Axe)) {
                CheckAndUpdateHeroPowerStatus (upgradePos);
            }
            break;
        case 2:
            if (GameUIHandlerTZ.weaponTypeUnlockStatus.Contains (WeaponsData.WeaponType.Sword)) {
                CheckAndUpdateHeroPowerStatus (upgradePos);
            }
            break;
        case 3:
            if (GameUIHandlerTZ.weaponTypeUnlockStatus.Contains (WeaponsData.WeaponType.Mace)) {
                CheckAndUpdateHeroPowerStatus (upgradePos);
            }
            break;
        case 4:
            if (GameUIHandlerTZ.weaponTypeUnlockStatus.Contains (WeaponsData.WeaponType.BattleHammer)) {

                CheckAndUpdateHeroPowerStatus (upgradePos);
            }
            break;
        case 5:

            if (GameUIHandlerTZ.weaponTypeUnlockStatus.Contains (WeaponsData.WeaponType.Rare)) {

                CheckAndUpdateHeroPowerStatus (upgradePos);
            }
            break;
        case 6:

            if (GameUIHandlerTZ.weaponTypeUnlockStatus.Contains (WeaponsData.WeaponType.Epic)) {
                CheckAndUpdateHeroPowerStatus (upgradePos);
            }
            break;
        case 7:
            if (GameUIHandlerTZ.weaponTypeUnlockStatus.Contains (WeaponsData.WeaponType.Legendary)) {
                CheckAndUpdateHeroPowerStatus (upgradePos);
            }
            break;
        }
    }

    public void HideWeapons (int index)
    {
        Transform nextWeaponPanel = weaponActivePanel.transform.GetChild (index);
        nextWeaponPanel .gameObject.SetActive (false);
    }
    public void DisplayWeapons (int index)
    {
        Transform nextWeaponPanels = weaponActivePanel.transform.GetChild (index);
        nextWeaponPanels .gameObject.SetActive (true);
    }
    public void HidePopups (int index)
    {
        Image[] popImages = weaponActivePanel.transform.GetChild (index).GetComponentsInChildren<Image> ();
        foreach (Image Img in popImages) {
            if (Img.name.Equals ("NoteWeaponImage")) {
                Img.gameObject.SetActive (false); 
            }
        }
    }
    public void DisplayPopups (int index)
    {
        Image[] popImages = weaponActivePanel.transform.GetChild (index).GetComponentsInChildren<Image> (true);
        Image openPanelweaponImage = openButtonPanel.GetComponent<RectTransform> ().FindChild ("btnOpenWeaponsPanel/NotificationImage").GetComponent<Image> ();
        foreach (Image Img in popImages) {
            if (Img.name.Equals ("NoteWeaponImage")) {
                if (WeaponsData.weaponData [index].m_Level == 0) {
                    Img.gameObject.SetActive (true); 
                    openPanelweaponImage.gameObject.SetActive (true);
                } else {
                    Img.gameObject.SetActive (false);
                    openPanelweaponImage.gameObject.SetActive (false);
                }
            }
        }
    }

    
    public void EnableWeaponPanel (int index)
    {
        Image[] MaskImages = weaponActivePanel.transform.GetChild (index).GetComponentsInChildren<Image> ();
        foreach (Image MaskImg in MaskImages) {
            if (MaskImg.name.Equals ("MaskPanel")) {
                MaskImg.gameObject.SetActive (false);
            }
        }
    }

    public void DisableWeaponPanel (int index)
    {
        Image[] MaskImages = weaponActivePanel.transform.GetChild (index).GetComponentsInChildren<Image> (true);
        foreach (Image MaskImg in MaskImages) {
            if (MaskImg.name.Equals ("MaskPanel")) {
                MaskImg.gameObject.SetActive (true);
            }
        }
    }

    public void ActivateOpenButtonPanel ()
    {
//        if (GameManagerTZ.totalGold >= 5) {
        Button heroButton = openButtonPanel.GetComponent<RectTransform> ().FindChild ("btnOpenHerosPanel").GetComponent <Button> ();
        Button weaponButton = openButtonPanel.GetComponent<RectTransform> ().FindChild ("btnOpenWeaponsPanel").GetComponent <Button> ();
        heroButton.gameObject.SetActive (true);
        weaponButton.gameObject.SetActive (true);
//        }
    }
    public void ActivateOpenRevenuePanel ()
    {
        //     Button artifactsButton = openButtonPanel.GetComponent<RectTransform> ().FindChild ("btnOpenArtifactsPanel").GetComponent <Button> ();
//        if (GameUIHandlerTZ.weaponTypeUnlockStatus.Count != 0) {
        Button revenueRectButton = RectPanel.GetComponent<RectTransform> ().FindChild ("revenueButton").GetComponent <Button> ();
        Button revenueButton = openButtonPanel.GetComponent<RectTransform> ().FindChild ("btnOpenRevenuePanel").GetComponent <Button> ();
        revenueRectButton.gameObject.SetActive (true);
        revenueButton.gameObject.SetActive (true);
//        }
    }

    void CheckAndUpdateHeroPowerStatus (int pos)
    {
        if (HeroPowersData.heroPowersData [pos].m_Cost <= GameManagerTZ.totalGold) {
            //Enable or Update Hero power
            /* first update staic variable with next values,
			 * if level 1 enable ui button and update corresponding value
			 * else just update corresponding value.
             */
            HeroPowersHandlerTZ.SkillType type = ConvertPosToSkill (pos);
            if (pos != 0) {
                if (HeroPowersData.heroPowersData [pos].m_Level == 0) {
                    // Enable image in powers Panel
//                    activePowersPanel.transform.gameObject.SetActive (true);
                    activePowersPanel.FindChild (HeroPowersData.heroPowersData [pos].m_Name).gameObject.SetActive (true);
                    inactivePowerspanel.FindChild (HeroPowersData.heroPowersData [pos].m_Name).gameObject.SetActive (true);

//                    Image openPanelheroImage = openButtonPanel.GetComponent<RectTransform> ().FindChild ("btnOpenHerosPanel/NotificationImage").GetComponent<Image> ();
//                    openPanelheroImage.gameObject.SetActive (false);
                    //					HeroPowersHandlerTZ.skills [type] = new HeroPowersHandlerTZ.ActiveSkill (type, (float)HeroPowersData.heroPowersData [pos].m_Increment,
//					                                                                         HeroPowersData.heroPowersData [pos].m_Timer);                  
                }
//                EnableHeroPanel (pos);
//				HeroPowersHandlerTZ.skills [type].damage += HeroPowersData.heroPowersData [pos].m_Increment;
            } else {
                PlayerData.UpdateBasicTapDamage (HeroPowersData.heroPowersData [pos].m_Increment);
            }
            GameManagerTZ.UpdateGold (-HeroPowersData.heroPowersData [pos].m_Cost);
            HeroPowersData.updateNextLevelData (pos, HeroPowersData.heroPowersData [pos].m_Level);
            gameUIHandler.CheckHeroPowersStatus ();
        } else {
            Vector3 VHero = new Vector3 (0f, -0.5f, -1.25f);
            PopDamageScript popupScript = CreatePopupString (VHero);
            popupScript.AttachString (string.Format (" Not enough Gold to Buy {0}", HeroPowersData.heroPowersData [pos].m_Name));
            popupScript.destructionTime = 10.0f;
            popupScript.risingSpeed = 0.6f;
            popupScript.displayText.fontSize = 12;
        }

    }

    public void OnWeaponUpgrade (int weaponPos)
    {
        if (WeaponsData.weaponData [weaponPos].m_Cost <= GameManagerTZ.totalGold) {
            /* update static variable with next gen values.
             * update corresponding things. equip corresponding weapon highlight the equipped weapon panel
             */
            gameManager.WeaponUpdatePurcahsed ();
            if (WeaponsData.weaponData [weaponPos].m_Level == 0) {
                heroPowersHandler.EquipWeapon (weaponPos);
                gameUIHandler.AddTypeToDic (WeaponsData.weaponData [weaponPos].m_Type);
                HidePopups (weaponPos);
                Image openPanelweaponImage = openButtonPanel.GetComponent<RectTransform> ().FindChild ("btnOpenWeaponsPanel/NotificationImage").GetComponent<Image> ();
                openPanelweaponImage.gameObject.SetActive (false);
            }
            GameManagerTZ.UpdateGold (-WeaponsData.weaponData [weaponPos].m_Cost);
            WeaponsData.updateNextLevelData (weaponPos, WeaponsData.weaponData [weaponPos].m_Level);
            heroPowersHandler.WeaponUpdated (weaponPos);
            gameUIHandler.CheckWeaponButtonStatus ();
        } else {
            Vector3 VWeapon = new Vector3 (0f, -0.5f, -1.25f);
            PopDamageScript popupScript = CreatePopupString (VWeapon);
            popupScript.AttachString (string.Format (" Not enough Gold to Buy {0}", WeaponsData.weaponData [weaponPos].m_Name));
            popupScript.destructionTime = 10.0f;
            popupScript.risingSpeed = 0.6f;
            popupScript.displayText.fontSize = 15;
        }
    }
    public void OnEquipWeapon (int weaponPos)
    {
        if (WeaponsData.weaponData [weaponPos].m_Level >= 1) {
            heroPowersHandler.EquipWeapon (weaponPos);
        } else {
            Vector3 VWeapon = new Vector3 (0f, -0.5f, -1.25f);
            PopDamageScript popupScript = CreatePopupString (VWeapon);
            popupScript.AttachString (string.Format ("Purchase {0} to Equip  ", WeaponsData.weaponData [weaponPos].m_Name));
            popupScript.destructionTime = 10.0f;
            popupScript.risingSpeed = 0.6f;
            popupScript.displayText.fontSize = 15;
        }
    }

    public void OnArtifactUpgrade (int artifactPos)
    {

    }

    public void OnRevenuePurchase (int purchaseButtonPos)
    {
        if (purchaseButtonPos > 4) {
            PurchaseStarDust (purchaseButtonPos);
        } else {
            UseSpecialDiamonds (purchaseButtonPos);
        }
    }

    private void PurchaseStarDust (int pos)
    {
//        switch (pos) {
//        case 5:
////            PurchasableItem p = Unibiller.AllPurchasableItems [0];
//            StoreInventory.BuyItem (StoreInfo.CurrencyPacks [pos - 5].ItemId);
//            break;
//        case 6:
//            StoreInventory.BuyItem (StoreInfo.CurrencyPacks [pos - 5].ItemId);
//            break;
//        case 7:
//            StoreInventory.BuyItem (StoreInfo.CurrencyPacks [pos - 5].ItemId);
//            break;
//        case 8:
//            StoreInventory.BuyItem (StoreInfo.CurrencyPacks [pos - 5].ItemId);
//            break;
//        case 9:
//            StoreInventory.BuyItem (StoreInfo.CurrencyPacks [pos - 5].ItemId);
//            break;
//        case 10:
//            StoreInventory.BuyItem (StoreInfo.CurrencyPacks [pos - 5].ItemId);
//            break;
//        }
    }

    private void UseSpecialDiamonds (int pos)
    {
        switch (pos) {
        case 0:
            if (GameManagerTZ.totalDiamonds >= 100) {
                gameManager.SetGoldChestMoney ();
                GameManagerTZ.UpdateDiamonds (-100);
            } else {
                // Display pop not enough StarDust.
            }
            break;
        case 1:
            if (GameManagerTZ.totalDiamonds >= 100) {
                gameManager.KillCurrentEnemy ();
                GameManagerTZ.UpdateDiamonds (-100);
            }
			//Kill monster.
            break;
        case 2:
			//Power of Taps, like 30 per second.
            GameManagerTZ.UpdateDiamonds (-50);
            break;
        case 3:
			//Shields Purchase
            GameManagerTZ.UpdateDiamonds (-100);
            break;
        case 4:
			//Refresh Skills
            float summOfCoolDownTimers = 0;
            foreach (KeyValuePair<HeroPowersHandlerTZ.SkillType, float> skillTimer in skillTimers) {
                summOfCoolDownTimers += skillTimer.Value;
            }
            int diamonds = (int)Mathf.Round (20.0f * Mathf.Log10 (summOfCoolDownTimers / 60.0f));
            if (GameManagerTZ.totalDiamonds >= diamonds) {
                foreach (KeyValuePair<HeroPowersHandlerTZ.SkillType, float> skillTimer in skillTimers) {
                    skillTimers.Remove (skillTimer.Key);
                }
                GameManagerTZ.UpdateDiamonds (-diamonds);
            }
            break;
        }
    }

    public void UpdateArtifactButton ()
    {
//		Button x = heroPowersPanel.GetComponent<RectTransform> ().FindChild ("upgradePanel/heroBaseDmgUpgrade/heroPurchaseButton").GetComponent <Button> ();
    }

    public void UpdateRevenueButton ()
    {
//		Button x = heroPowersPanel.GetComponent<RectTransform> ().FindChild ("upgradePanel/heroBaseDmgUpgrade/heroPurchaseButton").GetComponent <Button> ();
    }
    public void UpdateWeaponButton (string name, double cost, double dps, int level, bool isEnable)
    {
        Color updatedColor = new Color ();
        if (isEnable) {
            updatedColor = new Color32 (44, 170, 188, 255);
        } else {
            updatedColor = Color.gray;
        }
        Button purchButton = weaponsPanel.GetComponent<RectTransform> ().FindChild ("weaponsPanel/" + name + "/purchaseButton").GetComponent <Button> ();
        purchButton.image.color = updatedColor;
        foreach (Text childText in purchButton.GetComponentsInChildren <Text> ()) {
            if (childText.name.Equals ("ButtonCost")) {
                childText.text = LevelDataUtil.ToStringFormat (cost);
            } else {
                childText.text = "+ " + LevelDataUtil.ToStringFormat (dps) + " dps";
            }
        }
        Text lvlText = weaponsPanel.GetComponent<RectTransform> ().FindChild ("weaponsPanel/" + name + "/LvlText").GetComponent <Text> ();
        lvlText.text = "Lvl." + LevelDataUtil.ToStringFormat (level);
        Text skillText = weaponsPanel.GetComponent<RectTransform> ().FindChild ("weaponsPanel/" + name + "/SkillUpgradeText").GetComponent <Text> ();
        skillText.text = "Increase Base Damage By" + "  " + LevelDataUtil.ToStringFormat (dps);     
    }

    
    public void UpdateHeroButton (string name, double cost, double incr, int level, bool isEnable)
    {

        Color updatedColor = new Color ();
        Image heroPopupImage = heroPowersPanel.GetComponent<RectTransform> ().FindChild ("upgradePanel/" + name + "/NoteHeroImage").GetComponent<Image> ();
        if (isEnable) {
            Image openPanelheroImage = openButtonPanel.GetComponent<RectTransform> ().FindChild ("btnOpenHerosPanel/NotificationImage").GetComponent<Image> ();
            updatedColor = new Color32 (44, 170, 188, 255);
            Image heroMaskImage = heroPowersPanel.GetComponent<RectTransform> ().FindChild ("upgradePanel/" + name + "/MaskHeroPanel").GetComponent<Image> ();
            heroMaskImage.gameObject.SetActive (false);
            if (name.Equals ("heroBaseDmgUpgrade")) {
                if (level == 0) {
                    heroPopupImage.gameObject.SetActive (true);
                    openPanelheroImage.gameObject.SetActive (true);
                } else {
                    heroPopupImage.gameObject.SetActive (false);
                }
            } else {
                heroPopupImage.gameObject.SetActive (true);
                if (cost <= GameManagerTZ.totalGold) {
                    openPanelheroImage.gameObject.SetActive (true);
                }
            }
        } else {
            updatedColor = Color.gray;
            heroPopupImage.gameObject.SetActive (false);
        }
        Button purchButton = heroPowersPanel.GetComponent<RectTransform> ().FindChild ("upgradePanel/" + name + "/purchaseButton").GetComponent <Button> ();
        purchButton.image.color = updatedColor;
        foreach (Text childText in purchButton.GetComponentsInChildren <Text> ()) {
            if (childText.name.Equals ("ButtonCost")) {
                childText.text = LevelDataUtil.ToStringFormat (cost);
            } else {
                childText.text = "+ " + LevelDataUtil.ToStringFormat (incr);
            }
        }
        Text lvlText = heroPowersPanel.GetComponent<RectTransform> ().FindChild ("upgradePanel/" + name + "/LvlText").GetComponent <Text> ();
        lvlText.text = "Lvl." + LevelDataUtil.ToStringFormat (level);
        Text skillText = heroPowersPanel.GetComponent<RectTransform> ().FindChild ("upgradePanel/" + name + "/SkillUpgradeText").GetComponent <Text> ();
        skillText.text = "Increases Hero Damage By" + " " + LevelDataUtil.ToStringFormat (incr);  
    }
    
    public void UpdateAchievementButton (string name, double diaNum, bool isUpdated)
    {
        Color updatedColor = new Color ();
        if (isUpdated) {
            updatedColor = new Color32 (44, 170, 188, 255);
            AchievementPopupImage.gameObject.SetActive (isUpdated);
        } else {
            updatedColor = new Color32 (4, 4, 4, 224);
        }
        Button purchButton = achievementsPanel.GetComponent<RectTransform> ().FindChild ("ContentPanel/AchievementPanel/" + name + "/AchievementButton").GetComponent <Button> ();
        purchButton.image.color = updatedColor;
        Text buttonText = achievementsPanel.GetComponent<RectTransform> ().FindChild ("ContentPanel/AchievementPanel/" + name + "/AchievementButton/Text").GetComponent <Text> ();
        buttonText.text = diaNum.ToString () + "Diamonds"; 
    }
    public void UpdateAchievementText (string name, string AchieNum, bool isUpdated)
    {
        Text Achtext = achievementsPanel.GetComponent<RectTransform> ().FindChild ("ContentPanel/AchievementPanel/" + name + "/AchievementLvlText").GetComponent<Text> ();
        Achtext.text = AchieNum;
    }
    public void UpdateStarColor (string name, int stars, bool isUpdated)
    {
        Image UpdatedStar = achievementsPanel.GetComponent<RectTransform> ().FindChild ("ContentPanel/AchievementPanel/" + name + "/starsFillImage").GetComponent<Image> ();
        if (isUpdated) {
            UpdatedStar.fillAmount = (float)stars / 5; 
        }
    }
    public void UpdateNotificationImage (AchievementsData.Achievement tempAchievement, bool isunlock)
    {
        AchievementPopupImage.gameObject.SetActive (isunlock);
    }
	
	
    public void OnAchievementUnlock (int AchievementPos)
    { 
        AchievementsData.AchievementKey AchievementPosKey = (AchievementsData.AchievementKey)AchievementPos;
        if (AchievementsData.achievementsDictionary [AchievementPosKey].isUnlocked ()) {
            AchievementsData.achievementsDictionary [AchievementPosKey].IncrStarsListPos ();
            double diaIncr = AchievementsData.achievementsDictionary [AchievementPosKey].Diamonds [AchievementsData.achievementsDictionary [AchievementPosKey].m_Stars - 1];
            string lvlText = (AchievementsData.achievementsDictionary [AchievementPosKey].m_Value).ToString () + "/" + (AchievementsData.achievementsDictionary [AchievementPosKey].m_StarConstants [AchievementsData.achievementsDictionary [AchievementPosKey].m_Stars]).ToString ();
            GameManagerTZ.UpdateDiamonds (diaIncr);
            UpdateAchievementButton (AchievementsData.achievementsDictionary [AchievementPosKey].m_Name, AchievementsData.achievementsDictionary [AchievementPosKey].Diamonds [AchievementsData.achievementsDictionary [AchievementPosKey].m_Stars], true);
            UpdateStarColor (AchievementsData.achievementsDictionary [AchievementPosKey].m_Name, AchievementsData.achievementsDictionary [AchievementPosKey].m_Stars, true);
            UpdateAchievementText (AchievementsData.achievementsDictionary [AchievementPosKey].m_Name, lvlText, true);
            UpdateNotificationImage (AchievementsData.achievementsDictionary [AchievementPosKey], false);
        } else {
        }
        
    }

    
//    private void InitiateUniBill ()
//    {
//        Unibiller.onBillerReady += onBillerReady;
//        Unibiller.onTransactionsRestored += onTransactionsRestored;
//        Unibiller.onPurchaseCancelled += onCancelled;
//        Unibiller.onPurchaseFailed += onFailed;
//        Unibiller.onPurchaseCompleteEvent += onPurchased;
//        Unibiller.onPurchaseDeferred += onDeferred;
//        
//        // Now we're ready to initialise Unibill.
//        Unibiller.Initialise ();
//    }
//
    private void InitiateSoomla ()
    {
//        StoreEvents.OnSoomlaStoreInitialized += onSoomlaStoreInitialized;
//        StoreEvents.OnMarketPurchaseCancelled += onMarketPurchaseCancelled;
//        StoreEvents.OnUnexpectedStoreError += onUnexpectedStoreError;
//        StoreEvents.OnMarketPurchase += onMarketPurchase;
//        SoomlaStore.Initialize (new DiamondCrystalsData ());
//        SoomlaStore.StartIabServiceInBg ();
    }
//    private void onBillerReady (UnibillState state)
//    {
//        UnityEngine.Debug.Log ("onBillerReady:" + state);
//        UnibillError[] errors = Unibiller.Errors;
//        string error = "";
//        foreach (UnibillError err in errors) {
//            error += err.ToString ();
//        }
//
//        PopDamageScript popupScript = CreatePopupString (Vector3.zero);
//        popupScript.AttachString (string.Format ("Uni: {0}", error));
//        popupScript.destructionTime = 10.0f;
//        popupScript.risingSpeed = 0.0f;
//    }
    public void onSoomlaStoreInitialized ()
    {
//        UnityEngine.Debug.Log ("onBillerReady:" + state);
//        UnibillError[] errors = Unibiller.Errors;
//        string error = "";
//        foreach (UnibillError err in errors) {
//            error += err.ToString ();
//        }
//        
//        PopDamageScript popupScript = CreatePopupString (Vector3.zero);
//        popupScript.AttachString (string.Format ("Uni: {0}", error));
//        popupScript.destructionTime = 10.0f;
//        popupScript.risingSpeed = 0.0f;
    }
//    
//    /// <summary>
//    /// This will be called after a call to Unibiller.restoreTransactions().
//    /// </summary>
//    private void onTransactionsRestored (bool success)
//    {
//        Debug.Log ("Transactions restored.");
//    }
//    
//    /// <summary>
//    /// This will be called when a purchase completes.
//    /// </summary>
//    private void onPurchased (PurchaseEvent e)
//    {
//        Debug.Log ("Purchase OK: " + e.PurchasedItem.Id);
//        Debug.Log ("Receipt: " + e.Receipt);
//        Debug.Log (string.Format ("{0} has now been purchased {1} times.",
//                                  e.PurchasedItem.name,
//                                  Unibiller.GetPurchaseCount (e.PurchasedItem)));
////        GameManagerTZ.UpdateDiamonds (0);
//        switch (e.PurchasedItem.Id) {
//        case "com.pifrop.180bottlecaps":
//            GameManagerTZ.UpdateDiamonds (180);
//            break;
//        case "com.pifrop.500bottlecaps":
//            GameManagerTZ.UpdateDiamonds (500);
//            break;
//        case "com.pifrop.1200bottlecaps":
//            GameManagerTZ.UpdateDiamonds (1200);
//            break;
//        case "com.pifrop.3100bottlecaps":
//            GameManagerTZ.UpdateDiamonds (3100);
//            break;
//        case "com.pifrop.6500bottlecaps":
//            GameManagerTZ.UpdateDiamonds (6500);
//            break;
//        case "com.pifrop.14000bottlecaps":
//            GameManagerTZ.UpdateDiamonds (14000);
//            break;
//        }
//    }
//    public void onMarketPurchase (PurchasableVirtualItem pvi, string payload, Dictionary<string, string> extra)
//    {
//        Debug.Log ("Purchase OK: " + pvi.ItemId);
//        Debug.Log (string.Format ("{0} has now been purchased {1} times.",
//                                          pvi.Name,
//                                          "2"));
//        switch (pvi.ItemId) {
//        case "com.pifrop.170diam":
//            GameManagerTZ.UpdateDiamonds (180);
//            break;
//        case "com.pifrop.500diam":
//            GameManagerTZ.UpdateDiamonds (500);
//            break;
//        case "com.pifrop.1200diam":
//            GameManagerTZ.UpdateDiamonds (1200);
//            break;
//        case "com.pifrop.3100diam":
//            GameManagerTZ.UpdateDiamonds (3100);
//            break;
//        case "com.pifrop.6500diam":
//            GameManagerTZ.UpdateDiamonds (6500);
//            break;
//        case "com.pifrop.14000diam":
//            GameManagerTZ.UpdateDiamonds (14000);
//            break;
//        }
//    }
//
//    public void onMarketPurchaseCancelled (PurchasableVirtualItem pvi)
//    {
//        
//    }
//    /// <summary>
//    /// This will be called if a user opts to cancel a purchase
//    /// after going to the billing system's purchase menu.
//    /// </summary>
//    private void onCancelled (PurchasableItem item)
//    {
//        Debug.Log ("Purchase cancelled: " + item.Id);
//        GameManagerTZ.UpdateDiamonds (0);
//    }
//    
//    /// <summary>
//    /// iOS Specific.
//    /// This is called as part of Apple's 'Ask to buy' functionality,
//    /// when a purchase is requested by a minor and referred to a parent
//    /// for approval.
//    /// 
//    /// When the purchase is approved or rejected, the normal purchase events
//    /// will fire.
//    /// </summary>
//    /// <param name="item">Item.</param>
//    private void onDeferred (PurchasableItem item)
//    {
//        Debug.Log ("Purchase deferred blud: " + item.Id);
//    }
//    
//    /// <summary>
//    /// This will be called if an attempted purchase fails.
//    /// </summary>
//    private void onFailed (PurchasableItem item)
//    {
//        Debug.Log ("Purchase failed: " + item.Id);
//        GameManagerTZ.UpdateDiamonds (0);
//    }
    public void onUnexpectedStoreError (int errorCode)
    {
        Debug.Log ("Purchase failed: " + errorCode);
//        SoomlaUtils.LogError ("ExampleEventHandler", "error with code: " + errorCode);
    }
    public HeroPowersHandlerTZ.SkillType ConvertPosToSkill (int pos)
    {
        HeroPowersHandlerTZ.SkillType type = HeroPowersHandlerTZ.SkillType.ChargedStrike;
        switch (pos) {
        case 1:
            type = HeroPowersHandlerTZ.SkillType.ChargedStrike;
            break;
        case 2:
            type = HeroPowersHandlerTZ.SkillType.ArrowStrike;
            break;
        case 3:
            type = HeroPowersHandlerTZ.SkillType.CritChance;
            break;
        case 4:
            type = HeroPowersHandlerTZ.SkillType.AttackRate;
            break;
        case 5:
            type = HeroPowersHandlerTZ.SkillType.TapDamage;
            break;
        case 6:
            type = HeroPowersHandlerTZ.SkillType.GoldDrop;
            break;
        }
        return type;
    }
	
    public int ConvertSkillToPos (HeroPowersHandlerTZ.SkillType type)
    {
        int pos = 0;
        switch (type) {
        case HeroPowersHandlerTZ.SkillType.ChargedStrike:
            pos = 1;
            break;
        case HeroPowersHandlerTZ.SkillType.ArrowStrike:
            pos = 2;
            break;
        case HeroPowersHandlerTZ.SkillType.CritChance:
            pos = 3;
            break;
        case HeroPowersHandlerTZ.SkillType.AttackRate:
            pos = 4;
            break;
        case HeroPowersHandlerTZ.SkillType.TapDamage:
            pos = 5;
            break;
        case HeroPowersHandlerTZ.SkillType.GoldDrop:
            pos = 6;
            break;
        }
        return pos;
    }
}
