﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine.UI;
//[System.Serializable]
public class GameManagerTZ : MonoBehaviour
{
    
    /* It handles Gold, Enemy generation
     * 
     */
    public const string OFFLINECHEST = "offlineTreasureChest";
    public const string GOLDCHEST = "goldTreasureChest";

    // access to wave status & wave related variables
    public LevelManagerTZ lvlManager;
    public GameUIManagerTZ gameUIManager;
    public GoldSpawningScript goldSpawner;
    public GameUIHandlerTZ gameUiHandler;

    public GameObject offlineTreasureChest;
    public GameObject goldTreasureChest;
    // bools for track and call the data
    public static bool isTap3D = true;
    private static bool isButtonUIUpdated = true;
    private static bool isGoldDropTriggered = true;
    public static bool isEnemyLoaded;
    private static bool isLvlChanged = true;
    public static bool isFarming;
    public static string currentEnemyName;
    public static bool isAchievementReached;
    public static AchievementsData.Achievement tempAchievement;

    public static string chestName;

//	private static double kills = 0;
//	private double prevKills = 0;
    //Currencies
    private static double tempGold;
    public static double totalGold = 0;
    public static double currentDps;
    public static double weaponDps = 0;
    public static double totalDiamonds;
    public static double totalTaps = 0;
    private static double dpsReached;
    public static float goldIncr = 0;
    public static float treasureChestGoldIncr;
    private static bool isBossOrChest = false;


    //Enemy Related
    private int level ;
    private int noOfEnemiesPerLevel = 11;
    private bool isLvlMangerCalled = false;


    // Fairy Rewards
    private float fairyTime;
    private float lastFairyTime = float.MaxValue;
    public double elapsedTIme;
    public double exitTime;
    public static bool isChestTrigger;

    public HeroPowersHandlerTZ heroPowersHandler;
    private bool isHeroAttacking;
    private int currWeaponPos;

    //Particle Effects and Backgrounds
    public GameObject instaKillEffect;
    public Sprite[] Backgrounds;
    public SpriteRenderer photowood;

    // Save related
    private bool isSaved = false;
    private int noOfEnemiesLeft = 10;




    void Awake ()
    {
        offlineTreasureChest.SetActive (false);
        goldTreasureChest.SetActive (false);
        if (ES2.Exists ("level")) {
            int tempLevel = ES2.Load<int> ("level");
            if (tempLevel != 0) {
                level = tempLevel;
                isSaved = true;
                LoadData ();
            } else {
                InitializeValues ();
            }
        } else {
            InitializeValues ();
        }
//        InitializeValues ();
    }
    
    private void OnApplicationFocus (bool inBackground)
    {
        // if inBackground is true then game loses focus
        if (inBackground) {
//			SaveData ();
        } else {
//			LoadData ();
        }
    }
    
    private void OnApplicationQuit ()
    {
        SaveData ();
    }
    
    private void OnApplicationPause (bool isPause)
    {
//		SaveData ();
    }
    
    private void SaveData ()
    {
        GameData gameData = new GameData ();
        gameData.achievementsSave = AchievementsData.achievementsDictionary;
//		gameData.SkillTimersSave = gameUIManager.skillTimers;
        gameData.heroPowersSave = HeroPowersData.heroPowersData;
        gameData.weaponsSave = WeaponsData.weaponData;

        gameData.tapDamageSave = PlayerData.basicTapDamage;
        gameData.critChanceSave = PlayerData.criticalChance;
        gameData.critMultiplierMinSave = PlayerData.critMultiplierMin;
        gameData.critMultiplierMaxSave = PlayerData.critMultiplierMax;

        gameData.levelSave = LevelDataUtil.level;
        gameData.noOfLevelEnemiesSave = LevelDataUtil.noOfEnemies;
        gameData.enemyHealthSave = LevelDataUtil.enemyHealth;
        gameData.enemyLootDropSave = LevelDataUtil.enemyLootDrop;

        gameData.weaponTypeUnlockStatusSave = GameUIHandlerTZ.weaponTypeUnlockStatus;
        gameData.heroCurrentWeaponData = HeroPowersHandlerTZ.weaponPos;

        gameData.noOfEnemiesLeft = noOfEnemiesLeft;
        gameData.goldData = totalGold;
        gameData.diamondData = totalDiamonds;
        gameData.tapsData = totalTaps;
        if (noOfEnemiesLeft == 0) {
            gameData.isFarming = true;
        } else {
            gameData.isFarming = isFarming;
        }

        //Serialize and save.
        string serializedSave = JsonConvert.SerializeObject (gameData);
        Debug.Log (serializedSave);
        ES2.Save (serializedSave, "savedata.json");
        ES2.Save (level, "level");
        System.TimeSpan span = System.DateTime.Now.Subtract (new System.DateTime (1970, 1, 1, 0, 0, 0, 0));
        ES2.Save (span.TotalSeconds, "exitTime");
    }
    
    private void LoadData ()
    {
        string serializedLoad = ES2.Load<string> ("savedata.json");

        GameData loadData = JsonConvert.DeserializeObject<GameData> (serializedLoad);
        if (loadData.weaponTypeUnlockStatusSave.Count != 0) {
            isHeroAttacking = true;
        } else {
            isHeroAttacking = false;
        }
        GameUIHandlerTZ.weaponTypeUnlockStatus = loadData.weaponTypeUnlockStatusSave;
        AchievementsData.achievementsDictionary = loadData.achievementsSave;
//		gameUIManager.skillTimers =loadData.SkillTimersSave;
        HeroPowersData.heroPowersData = loadData.heroPowersSave;
        WeaponsData.weaponData = loadData.weaponsSave;

        PlayerData.basicTapDamage = loadData.tapDamageSave;
        PlayerData.criticalChance = loadData.critChanceSave;
        PlayerData.critMultiplierMin = loadData.critMultiplierMinSave;
        PlayerData.critMultiplierMax = loadData.critMultiplierMaxSave;

        totalGold = loadData.goldData;
        totalDiamonds = loadData.diamondData;
        totalTaps = loadData.tapsData;
        isFarming = loadData.isFarming;
        currWeaponPos = loadData.heroCurrentWeaponData;
        noOfEnemiesLeft = loadData.noOfEnemiesLeft;
        System.TimeSpan span = System.DateTime.Now.Subtract (new System.DateTime (1970, 1, 1, 0, 0, 0, 0));
        elapsedTIme = span.TotalSeconds - ES2.Load<double> ("exitTime");
        exitTime = ES2.Load<double> ("exitTime");
    }
    
    void Start ()
    {
        StartGame ();
    }

    void Update ()
    {
        if (isAchievementReached) {
            gameUiHandler.UpdateAllAchievementStatus ();
        } else {
            isAchievementReached = false;
        }

        if (Input.GetKeyDown (KeyCode.Escape)) {
//            SaveData ();
            Application.Quit ();
        }

        // Level Manager Check
        if (!isEnemyLoaded && !isLvlMangerCalled) {
            LevelManagerTZ.LevelInfo lvlInfo = lvlManager.LoadEnemy (isFarming);
            isLvlMangerCalled = true;
            isEnemyLoaded = true;
            noOfEnemiesLeft = lvlInfo.noOfEnemiesLeft;
            UpdateStatus ();
//            if (lvlInfo.isLevelChanged) {
//                LevelChanged ();
//                gameUIManager.UpdateBossButtonStatus (false, false);
//            }

        } else {
            isLvlMangerCalled = false;
        }

        if (!isLvlChanged) {
            isLvlChanged = true;
            LevelChanged ();
        }

        if ((Time.time - lastFairyTime) > fairyTime) {
            TriggerFairyChest (true);
        }

        // Gold drop Check;
        if (!isGoldDropTriggered) {
            isGoldDropTriggered = true;
            PopDamageScript popupScript = GameUIManagerTZ.CreatePopupString (Vector3.zero);
//            popupScript.AttachString ("In Drop Check");
            popupScript.destructionTime = 25.0f;
            DropGold (tempGold);
        }

        // UI button updating
        if (!isButtonUIUpdated) {
            gameUiHandler.CheckWeaponButtonStatus ();
            gameUiHandler.CheckHeroPowersStatus ();
            TriggerPanels ();
            isButtonUIUpdated = true;
        }

        if (isChestTrigger) {
            isChestTrigger = false;
            OnChestClick ();
        }
    }

    private void InitializeValues ()
    {
        isSaved = false;
        level = 0;
        PlayerData.basicTapDamage = 1;
        PlayerData.criticalChance = 4;
        HeroPowersData.InitializeHeroPowers ();
        heroPowersHandler.InitializeWeapons ();
        UpdateGold (0);
    }

    private void StartGame ()
    {
        UpdateGold (0);
        if (isHeroAttacking) {
            heroPowersHandler.EquipWeapon (currWeaponPos);
        }
        if (isSaved) {
            gameUiHandler.UpdateAllAchievementStatus ();
            isEnemyLoaded = true;
            lvlManager.PrepareLevel (level, noOfEnemiesPerLevel, noOfEnemiesLeft + 1);
            lvlManager.LoadEnemy (isFarming);
            int nextlevel = level + 1;
            gameUIManager.UpdateGameLevel (level.ToString (), nextlevel.ToString ());
            UpdatefillAmount (false);
            UpdateStatus ();
            if (elapsedTIme > 1 && heroPowersHandler.heroCurrDps != 0) {
                // Enable Chest Button
                offlineTreasureChest.SetActive (true);
                double offlineGold = GetOfflineGOld (elapsedTIme, heroPowersHandler.heroCurrDps);
                TreasureChestScript cScript = offlineTreasureChest.GetComponent <TreasureChestScript> ();
                cScript.gold = offlineGold;
            }
        } else {
            isEnemyLoaded = true;
//            InitiateLevel ();
            LevelChanged ();
        }
        InvokeRepeating ("ResetDps", 1.0f, 1.0f);
//        totalDiamonds = 5E+2;
//        totalGold = 5E+5;
//        totalDiamonds = 0;
//        totalGold = 50E+10;
        gameUiHandler.CheckWeaponButtonStatus ();
        gameUiHandler.CheckHeroPowersStatus ();
    }

    private void ResetDps ()
    {
        currentDps = System.Math.Round (heroPowersHandler.heroCurrDps);

    }

    public void SetGoldChestMoney ()
    {
        goldTreasureChest.SetActive (true);
        double treasureGold = LevelDataUtil.enemyLootDrop * 500;
        TreasureChestScript cScript = goldTreasureChest.GetComponent <TreasureChestScript> ();
        cScript.gold = treasureGold;
    }

    private void OnChestClick ()
    {

        if (chestName.Equals (OFFLINECHEST)) {
            TreasureChestScript cScript = offlineTreasureChest.GetComponent <TreasureChestScript> ();
            cScript.OpenSesame ();
            chestName = string.Empty;
        } else if (chestName.Equals (GOLDCHEST)) {
            TreasureChestScript cScript = goldTreasureChest.GetComponent <TreasureChestScript> ();
            cScript.OpenSesame ();
            chestName = string.Empty;
        }
    }

    private double GetOfflineGOld (double elapsedTime, double dps)
    {
        double offlineGold = ((LevelDataUtil.enemyLootDrop * Mathf.Min ((float)elapsedTime, 3 * 60 * 60 * 24.0f)) / ((LevelDataUtil.enemyHealth / dps) + 0.5));
        return offlineGold;
    }

    public void KillCurrentEnemy ()
    {
// Insta kill FX effect.
        EnemyPropsTZ aiScript = PoolManager.Props [currentEnemyName];
        aiScript.InstaKillEnemy ();
        if (instaKillEffect)
            PoolManager.Pools ["ParticleEffects"].Spawn (instaKillEffect,
                                                         new Vector3 (aiScript.gameObject.transform.position.x,
                         aiScript.gameObject.transform.position.y,
                         aiScript.gameObject.transform.position.z), Quaternion.Euler (-90, 0, 0));

    }

    public void InitiateLevel ()
    {
//		if (level >= 6) {
////			TriggerFairyChest ();
//		}
        level += 1;
        lvlManager.PrepareLevel (level, noOfEnemiesPerLevel, noOfEnemiesLeft + 1);
        lvlManager.LoadEnemy (isFarming);
        string enemiesLeftInfo = (noOfEnemiesPerLevel - 1 - noOfEnemiesLeft).ToString () + "/" + (noOfEnemiesPerLevel - 1).ToString ();
        gameUIManager.UpdateBossButtonStatus (false, false);
        gameUIManager.UpdateEnemyNumber (enemiesLeftInfo);
        UpdatefillAmount (false);
    }

    private void LevelChanged ()
    {
        level += 1;
        noOfEnemiesLeft = noOfEnemiesPerLevel - 1;
        lvlManager.PrepareLevel (level, noOfEnemiesPerLevel, noOfEnemiesLeft + 1);
        lvlManager.LoadEnemy (isFarming);
        string enemiesLeftInfo = (noOfEnemiesPerLevel - 1 - noOfEnemiesLeft).ToString () + "/" + (noOfEnemiesPerLevel - 1).ToString ();
        gameUIManager.UpdateBossButtonStatus (false, false);
        gameUIManager.UpdateEnemyNumber (enemiesLeftInfo);
        gameUIManager.LevelRectRenderer.fillAmount = 0f;
        int nextlevel = level + 1;
        gameUIManager.UpdateGameLevel (level.ToString (), nextlevel.ToString ());
//        AchievementsData.levelreached.m_Value += 1;
//        tempAchievement = AchievementsData.levelreached;
        AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.LevelReached].m_Value += 1;
        tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.LevelReached];
        isAchievementReached = true;
        int stageNumber = (int)level / 5;
        int stageRemainder = level % 5;
        int backgroundNumber = stageNumber % 7;
        if (stageRemainder == 0) {
            photowood.sprite = Backgrounds [backgroundNumber];
        }
//        InitiateLevel ();
    }

    private void UpdateStatus ()
    {
        if (isFarming) {
            gameUIManager.UpdateBossButtonStatus (true, true);
            UpdatefillAmount (true);
        } else {
            if (noOfEnemiesLeft == 0) {
                gameUIManager.UpdateBossButtonStatus (false, true);
                UpdatefillAmount (true);
            } else {
                int remainingEnemies = noOfEnemiesPerLevel - noOfEnemiesLeft - 1;
                if (remainingEnemies < 0)
                    remainingEnemies = 0;
                string enemiesLeftInfo = (remainingEnemies).ToString () + "/" + (noOfEnemiesPerLevel - 1).ToString ();
                UpdatefillAmount (false);
                gameUIManager.UpdateBossButtonStatus (false, false);
                gameUIManager.UpdateEnemyNumber (enemiesLeftInfo);
            }
        }
    }
    
    public void PrepareToLoseCondition ()
    {
    }

//    public static void UpdateTaps (double tapsIncr)
//    {
//        totalTaps += tapsIncr;
//        if (tapsIncr > 0) {
//            AchievementsData.noOfTaps.m_Value += tapsIncr;
//            tempAchievement = AchievementsData.noOfTaps;
//            isAchievementReached = true;
//        }
//    }

    public static void UpdateGold (double goldIncrement)
    {
        totalGold += goldIncrement;
        if (goldIncrement > 0) {
//            AchievementsData.totalGoldCollected.m_Value += goldIncrement;
//            tempAchievement = AchievementsData.totalGoldCollected;
            AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.TotalGoldCollected].m_Value += goldIncrement;
            tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.TotalGoldCollected];
            isAchievementReached = true;
        }
        isButtonUIUpdated = false;
    }
//Diamonds Update and Rewards
    public static void UpdateDiamonds (double diamondIncrement)
    {
        totalDiamonds += diamondIncrement;
    }

    private void TriggerFairyChest (bool isFairy)
    {
//        FairyRewards fairyRewards = treasureChest.GetComponent<FairyRewards> ();
//        if (isFairy) {
//            lastFairyTime = Time.time;
//            fairyTime = Random.Range (62, 120);
//            Animator chestAnimator = treasureChest.GetComponent<Animator> ();
//            chestAnimator.SetTrigger ("Open");
//            // Make Chest clickable and trigger fair reward once it is clicked
//            
//            int rewardType = fairyRewards.GenerateReward ();
//            if (rewardType == 0) {
//                //Drop Gold
//            } else {
//                heroPowersHandler.OnSkillPress (rewardType);
//            }
//        } else {
//
//        }
    }

    public static void HitInfo (double tapDamage, bool isCrit)
    {
        currentDps += tapDamage;
        if (isCrit) {
            AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.CriticalHits].m_Value += 1;
            tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.CriticalHits];
        } else {
            AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.Hits].m_Value += 1;
            tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.Hits];
        }
        if (currentDps > dpsReached) {
            dpsReached = currentDps;
            AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.DpsReached].m_Value = dpsReached;
            tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.DpsReached];
        }
        isAchievementReached = true;
    }

    public void WeaponUpdatePurcahsed ()
    {
        AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.WeaponUpgrades].m_Value += 1;
        tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.WeaponUpgrades];
        isAchievementReached = true;
    }

    public void ChargeUsed ()
    {
        AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.Charges].m_Value += 1;
        tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.Charges];
        isAchievementReached = true;
    }

    public static void EnemyKilled (bool isBoss)
    {
        if (isBoss) {
//            AchievementsData.noOfBossesKilled.m_Value += 1;
//            tempAchievement = AchievementsData.noOfBossesKilled;
            AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.BossesKilled].m_Value += 1;
            tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.BossesKilled];
            isAchievementReached = true;
            isFarming = false;
            isLvlChanged = false;
        } else {
            AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.MonstersKilled].m_Value += 1;
            tempAchievement = AchievementsData.achievementsDictionary [AchievementsData.AchievementKey.MonstersKilled];
            isAchievementReached = true;
            isEnemyLoaded = false;
        }

    }

    public void UpdatefillAmount (bool isBoss)
    {
        if (isBoss) {
            gameUIManager.LevelRectRenderer.fillAmount = 1f;
        } else {
            gameUIManager.LevelRectRenderer.fillAmount = (float)(noOfEnemiesPerLevel - 1 - noOfEnemiesLeft) / (noOfEnemiesPerLevel - 1);
        }
    }

    public  void TriggerPanels ()
    {
        gameUIManager.ActivateOpenButtonPanel ();
        gameUIManager.ActivateOpenRevenuePanel ();
        int j = 0;
        for (int i=0; i<WeaponsData.weaponsCost.Count; i++) {
            if (WeaponsData.weaponsCost [i] >= totalGold) {
                if (WeaponsData.weaponData [i].m_Level == 0) {
                    j += 1;
                    gameUIManager.HidePopups (i);
                    if (j == 1)
                        continue;
                    gameUIManager.HideWeapons (i);
                } 
            } else {
                gameUIManager.DisplayWeapons (i);
                gameUIManager.DisplayWeapons (i + 1);
                gameUIManager.DisplayPopups (i);
                gameUIManager.EnableWeaponPanel (i);
            }

        }


    }

    public static void DropFinalEnemyMoney (double initialMoney, bool isBoss)
    {
        isGoldDropTriggered = false;
        if (isBoss) {
            isBossOrChest = true;
        } else {
            isBossOrChest = false;
        }
        tempGold = initialMoney * (1 + goldIncr);
    }

    public static void DropEnemyChestMoney (double chestMoney)
    {
        isGoldDropTriggered = false;
        tempGold = chestMoney * (treasureChestGoldIncr);
        isBossOrChest = true;
    }

    public static void DropTreasureMoney (double treasureMoney)
    {
        isGoldDropTriggered = false;
        tempGold = treasureMoney;
        isBossOrChest = true;
    }

    private void DropGold (double gold)
    {
        goldSpawner.SpawnGold (gold, new Vector3 (0, 1, 0), isBossOrChest);
    }
    
    public static void FailedToKillBoss ()
    {
        isFarming = true;
        isEnemyLoaded = false;
        GameUIManagerTZ.BossFailMessage ();
    }
}
