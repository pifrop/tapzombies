using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManagerTZ : MonoBehaviour
{
    /* Generates levels adn set enemies properties accordingly, handles rewards.
     * Level can have many waves 
     * 
     */
    public GameObject[] enemyPrefabs;

    private int prevBossPrefabPos = 0;
    private int failedBossPrefabPos = 0;
    private Vector2 spawnPos = Vector2.zero;
    private Vector3 spawnPos3D = Vector3.zero;
    public int noOfEnemiesInWave = 11;

    public struct LevelInfo
    {
        public bool isLevelChanged;
        public int noOfEnemiesLeft;
        public LevelInfo (bool isChanged, int enemiesLeft)
        {
            noOfEnemiesLeft = enemiesLeft;
            isLevelChanged = isChanged;
        }
    }

    public void PrepareLevel (int level, int noOfEnemies, int noOfEnemiesLeft)
    {
        noOfEnemiesInWave = noOfEnemiesLeft;
        double health = 18.5f * System.Math.Pow (1.57f, Mathf.Min (level, 156)) * System.Math.Pow (1.17f, Mathf.Max (0, level - 156));
        double moneyDrop = System.Math.Round ((float)health * (0.02f + (0.00045f * Mathf.Min (level, 150))));
        if (moneyDrop == 0) {
            moneyDrop = 1;
        }
        LevelDataUtil.level = level;
        LevelDataUtil.noOfEnemies = noOfEnemies;
        LevelDataUtil.enemyHealth = health;
        LevelDataUtil.enemyLootDrop = moneyDrop;
        switch (level % 5) {
        case 1:
            LevelDataUtil.bossMultiplier = 2;
            break;
        case 2:
            LevelDataUtil.bossMultiplier = 4;
            break;
        case 3:
            LevelDataUtil.bossMultiplier = 6;
            break;
        case 4:
            LevelDataUtil.bossMultiplier = 7;
            break;
        case 5:
            LevelDataUtil.bossMultiplier = 10;
            break;
        default:
            LevelDataUtil.bossMultiplier = 2 * (level % 5);
            break;
        }
//        noOfEnemiesInWave -= 1;
//        SpawnEnemy (EnemyPropsTZ.EnemyType.Normal);
    }

    public void BuildWave ()
    {

    }

    public void SpawnEnemy (EnemyPropsTZ.EnemyType type)
    {
        int enemySelection = Random.Range (0, enemyPrefabs.Length);
        if ((type == EnemyPropsTZ.EnemyType.Boss) && (failedBossPrefabPos != 0)) {
            enemySelection = failedBossPrefabPos;
            failedBossPrefabPos = 0;
        }
        GameObject asa = enemyPrefabs [enemySelection];
        GameObject enemy = PoolManager.Pools ["Enemies"].Spawn (enemyPrefabs [enemySelection], new Vector3 (0f, -.5f, 0), Quaternion.Euler (0, 180f, 0));
        EnemyPropsTZ aiScript = PoolManager.Props [enemy.name];
        GameManagerTZ.currentEnemyName = enemy.name;
        if (type == EnemyPropsTZ.EnemyType.Boss) {
            aiScript.maxHealth = LevelDataUtil.bossMultiplier * (LevelDataUtil.enemyHealth);
            aiScript.normCurrDrop = LevelDataUtil.bossMultiplier * (LevelDataUtil.enemyLootDrop);
            prevBossPrefabPos = enemySelection;
        } else {
            aiScript.maxHealth = LevelDataUtil.enemyHealth;
            aiScript.normCurrDrop = LevelDataUtil.enemyLootDrop;
        }
        aiScript.enemyType = type;
        aiScript.PrepareEnemy ();
    }

    public void SpawnWave ()
    {

    }

    public LevelInfo LoadEnemy (bool isFarming)
    {
        if (isFarming) {
            if (prevBossPrefabPos != 0) {
                failedBossPrefabPos = prevBossPrefabPos;
                prevBossPrefabPos = 0;
            }
            SpawnEnemy (EnemyPropsTZ.EnemyType.Normal);
            noOfEnemiesInWave = 1;
            return new LevelInfo (false, noOfEnemiesInWave);
        } else {
            bool isLevelChanged;
            if (noOfEnemiesInWave > 0) {
                noOfEnemiesInWave -= 1;
                isLevelChanged = false;
                if (noOfEnemiesInWave == 0) {
                    SpawnEnemy (EnemyPropsTZ.EnemyType.Boss);
                } else {
                    SpawnEnemy (EnemyPropsTZ.EnemyType.Normal);
                }
            } else {
                //			noOfEnemiesInWave = 11;
                isLevelChanged = true;
            }
            LevelInfo lvlInfo = new LevelInfo (isLevelChanged, noOfEnemiesInWave);
            return lvlInfo;
        }
    }
}