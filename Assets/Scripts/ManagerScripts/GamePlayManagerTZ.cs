﻿using UnityEngine;
using System.Collections;

public class GamePlayManagerTZ : MonoBehaviour
{
    /* It does all the game play processing for the input actions
     * 
     */
	
    public void EnemyInput (InputHandlerTZ.InputType type, string name, Vector2 dirVec, float strikeDis, Vector3 startPos)
    {
        EnemyPropsTZ aiScript = PoolManager.Props [name];
        switch (type) {
        case InputHandlerTZ.InputType.Tap:
            aiScript.Hit (PlayerData.GetFinalTapDamage (), dirVec, startPos, startPos + (new Vector3 (dirVec.x * 4, dirVec.y * 4, 0.0f)));
            break;
        case InputHandlerTZ.InputType.Slash:
//            aiScript.Hit (PlayerData.GetFinalTapDamage (), dirVec, startPos, startPos + (new Vector3 (dirVec.x * 4, dirVec.y * 4, 0.0f)));
            break;
        case InputHandlerTZ.InputType.DoubleTap:
            break;
        case InputHandlerTZ.InputType.LongTouch:
            break;
        }
    }
	
    public void GoldCoinInput (InputHandlerTZ.InputType type, string name)
    {
        CoinScript coinScript = PoolManager.CoinProps [name];
        switch (type) {
        case InputHandlerTZ.InputType.Tap:
            coinScript.TapRemoveCoin ();
            break;
        case InputHandlerTZ.InputType.Slash:
//            coinScript.TapRemoveCoin ();
            break;
        case InputHandlerTZ.InputType.DoubleTap:
            break;
        case InputHandlerTZ.InputType.LongTouch:
            break;
        }
    }

    public void ChestInput (InputHandlerTZ.InputType type, string name)
    {

        switch (type) {
        case InputHandlerTZ.InputType.Tap:
            GameManagerTZ.isChestTrigger = true;
            GameManagerTZ.chestName = name;
            break;
        case InputHandlerTZ.InputType.Slash:
            break;
        case InputHandlerTZ.InputType.DoubleTap:
            break;
        case InputHandlerTZ.InputType.LongTouch:
            break;
        }
    }
	
    public void NoColliderInput (InputHandlerTZ.InputType type)
    {
        switch (type) {
        case InputHandlerTZ.InputType.Tap:
            break;
        case InputHandlerTZ.InputType.Slash:
            break;
        case InputHandlerTZ.InputType.DoubleTap:
            break;
        case InputHandlerTZ.InputType.LongTouch:
            break;
        }
    }
}
