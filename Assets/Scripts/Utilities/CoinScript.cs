﻿using UnityEngine;
using System.Collections;

public class CoinScript : MonoBehaviour
{

    public double value;
    private float lastHitTime = 0.0f;
    private Rigidbody rigidBody;	
    public  Vector3[] forceVectors = new Vector3[] {
		new Vector3 (),
        new Vector3 (),
        new Vector3 (),
        new Vector3 (),
        new Vector3 (),
        new Vector3 (),
        new Vector3 (),
        new Vector3 (),

	}; 

    void Start ()
    {
        rigidBody = GetComponent <Rigidbody> ();
        int k = forceVectors.Length;
        float forceIndex = Random.Range (0, forceVectors.Length);
        //      rigidBody.constraints = RigidbodyConstraints.FreezePositionZ;
        rigidBody.AddForce (forceVectors [(int)forceIndex]);
//        rigidBody.AddForce (forceVectors [6]);
        Invoke ("RemoveCoin", 5.0f);
    }


    void OnCollisionEnter (Collision collision)
    {
        if (collision.collider.gameObject.name.Equals ("Ground")) {
            Invoke ("SetCoinRotation", 0.0f);
            InvokeRepeating ("RotateCoin", 0.5f, 0.1f);
        }
//        if (collision.collider.gameObject.name.Equals ("GoldCoin")) {
//            Invoke ("SetCoinRotation", 0.0f);
//            InvokeRepeating ("RotateCoin", 0.5f, 0.1f);
//        }
    }

    void SetCoinRotation ()
    {
        rigidBody.isKinematic = true;
        rigidBody.useGravity = false;
    }

    void RotateCoin ()
    {
        gameObject.transform.RotateAround (this.transform.localPosition, Vector3.up, 15);
    }

    IEnumerator OnSpawn ()
    {   
        if (!PoolManager.Props.ContainsKey (gameObject.name))
            PoolManager.CoinProps.Add (gameObject.name, this);
        yield return new WaitForEndOfFrame ();
    }

    public void TapRemoveCoin ()
    {
        if (IsInvoking ("RemoveCoin")) {
            CancelInvoke ("RemoveCoin");
        }
        RemoveCoin ();
    }

    private void RemoveCoin ()
    {
        Vector3 modPos = new Vector3 (transform.position.x - 0.2f, transform.position.y, transform.position.z + 0.3f);
        PopDamageScript popupScript = GameUIManagerTZ.CreatePopupString (transform.position);
        popupScript.AttachString (LevelDataUtil.ToStringFormat (value));
        popupScript.destructionTime = 2.0f;
        popupScript.risingSpeed = 0.5f;
        popupScript.displayText.color = new Color32 (255, 223, 0, 255);
        GameManagerTZ.UpdateGold (value);
        Destroy (gameObject);
    }

}
