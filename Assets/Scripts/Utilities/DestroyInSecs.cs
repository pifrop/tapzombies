﻿using UnityEngine;
using System.Collections;

public class DestroyInSecs : MonoBehaviour
{
	IEnumerator OnSpawn ()
	{   
		Destroy (gameObject, 2.0f); // If added animation in this wait for end of the animation.
		yield return new WaitForEndOfFrame ();
	}

	void Start ()
	{
		Destroy (gameObject, 2.0f);
	}
}
