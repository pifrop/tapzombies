﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopDamageScript : MonoBehaviour
{
    public float risingSpeed = 0.5f;
    public float destructionTime = 2.0f;
    public Text displayText;

    void Update ()
    {
        transform.Translate (Vector3.up * Time.deltaTime * risingSpeed);
    }

    IEnumerator OnSpawn ()
    {   
        Destroy (gameObject, destructionTime); // If added animation in this wait for end of the animation.
        yield return new WaitForEndOfFrame ();
    }
    
    void Start ()
    {
        Destroy (gameObject, destructionTime);
    }

    public void AttachString (string dispText)
    {
        displayText = gameObject.GetComponentInChildren<Text> ();
        displayText.text = dispText;
    }
}
