﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GoldSpawningScript : MonoBehaviour
{

    private List<int> noOfCoinsToDropList = new List<int> (new int[] {3, 4, 5});
    public GameObject coin;
    public int noOfCoinsToDrop;
    public Vector3 position;
    public double coinValue;
    void Update ()
    {

    }
	
    public void SpawnGold (double totalAmount, Vector3 position, bool isBossOrChest)
    {
        if (totalAmount < 6) {
            if (totalAmount < 2) {
                noOfCoinsToDrop = 1;
            } else if (totalAmount < 4) {
                noOfCoinsToDrop = 2;
            } else {
                noOfCoinsToDrop = 4;
            }
            coinValue = 1;
        } else {
            if (isBossOrChest) {
                noOfCoinsToDrop = Random.Range (12, 14);
            } else {
                noOfCoinsToDrop = noOfCoinsToDropList [Random.Range (0, noOfCoinsToDropList.Count)];
            }
            coinValue = System.Math.Floor (totalAmount / noOfCoinsToDrop);
            if (coinValue == 0) {
                coinValue = 1;
            }
        }
        InvokeRepeating ("SpawnGoldInOrder", 0f, .2f);
   
    }
    public void SpawnGoldInOrder ()
    {
        if (noOfCoinsToDrop == 0) {
            CancelGoldSpawn ();
        } else {
            noOfCoinsToDrop -= 1;
            GameObject currentCoin = PoolManager.Pools ["ParticleEffects"].Spawn (coin, new Vector3 (position.x, position.y, position.z - 0.5f), Quaternion.identity);
            CoinScript coinScript = currentCoin.GetComponent<CoinScript> ();
            coinScript.value = coinValue;
        }
    }
    public void CancelGoldSpawn ()
    {
        CancelInvoke ("SpawnGoldInOrder");
    }
}

