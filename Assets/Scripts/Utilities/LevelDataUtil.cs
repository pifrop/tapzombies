﻿using UnityEngine;
using System.Collections;

public static class LevelDataUtil
{
    public static int level;
    public static int noOfEnemies;
    public static double enemyHealth;
    public static double enemyLootDrop;
    public static int bossMultiplier;

    public static string ToStringFormat (double num)
    {
        if (num <= 0) {
            num = 0;
        }
        if (num <= 1000) {
            num = Mathf.RoundToInt ((float)num);
        }
        double exp = System.Math.Log (System.Math.Abs (num), 1000);
        int expInt = (int)System.Math.Floor (exp);
        string notation = "";
        int asciiChar = 0;
        if ((expInt < 5) && (expInt > 0)) {
            switch (expInt) {
            case 1:
                asciiChar = 75;
                break;
            case 2:
                asciiChar = 77;
                break;
            case 3:
                asciiChar = 66;
                break;
            case 4:
                asciiChar = 84;
                break;
            default:
                break;
            }
        } else if (expInt >= 5) {
            asciiChar = 92 + expInt;
        } else {
            asciiChar = 0;
        }
        if (asciiChar != 0) {
            notation = ((char)asciiChar).ToString ();
        }
        string finInt = "";
        if (num == 0) {
            finInt = "0";
        } else if (num < 1000) {
            finInt = num.ToString ();
        } else {
            finInt = (num / (System.Math.Pow (10, expInt * 3))).ToString ("F2");
        }
        return string.Concat (finInt, notation);
    }
}
