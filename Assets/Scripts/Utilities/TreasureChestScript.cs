﻿using UnityEngine;
using System.Collections;

public class TreasureChestScript : MonoBehaviour
{
    public double gold;
    private Quaternion initialRotation;
    private bool isClicked = false;
    private bool isOpened = false;
    private float removeTime;

    void Awake ()
    {
        initialRotation = gameObject.transform.rotation;
    }

    void OnEnable ()
    {
        InvokeRepeating ("RotateCoin", 0.5f, 0.1f);
    }

    void OnDisable ()
    {
        if (IsInvoking ("RotateCoin"))
            CancelInvoke ("RotateCoin");
        gameObject.transform.rotation = initialRotation;
    }


    public void OpenSesame ()
    {
//        StartCoroutine ("OpenChest");
        CancelInvoke ("RotateCoin");
        gameObject.transform.rotation = initialRotation;
        Animator animator = gameObject.GetComponent<Animator> ();
        animator.SetTrigger ("Open");
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo (0);
        GameManagerTZ.DropTreasureMoney (gold);
        Invoke ("RemoveChest", 1.2f);
    }

    void RemoveChest ()
    {
        gameObject.SetActive (false);
    }

    void RotateCoin ()
    {
//        if (isClicked) {
//                CancelInvoke ("RotateCoin");
//                StartCoroutine ("OpenChest");
//                RemoveChest ();
//        }
        gameObject.transform.RotateAround (this.transform.localPosition, Vector3.up, 15);
    }
//
//
//    IEnumerator OpenChest ()
//    {
//
////        return stateInfo.length;
//        yield return new WaitForSeconds (stateInfo.length);
//    }
}
