﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroPowersData : MonoBehaviour
{
    public class HeroPower
    {
        public string m_Name;
        public double m_Cost;
        public double m_Increment;
        public int m_Level;
        public double m_BaseIncrement;
        public float m_Timer;
        public float m_Cooldown;
		
        public HeroPower (string name, double incr, double cost, double baseIncr, float coolDownTime, float activeTimer, int level)
        {
            m_Name = name;
            m_Increment = incr;
            m_Cost = cost;
            m_BaseIncrement = baseIncr;
            m_Cooldown = coolDownTime;
            m_Level = level;
            m_Timer = activeTimer;
        }
    }
    public static double prevPlayerBasicTapDamage = 4;
    public static List<HeroPower> heroPowersData = new List<HeroPower> ();

    void Awake ()
    {

    }

    public static void InitializeHeroPowers ()
    {
        heroPowersData.Add (new HeroPower ("heroBaseDmgUpgrade", 1, 5, 0, 0, 0, 1));
        heroPowersData.Add (new HeroPower ("singleStrikeUpgrade", 140, 2.78E+3, 70, 600, 3, 0));
        heroPowersData.Add (new HeroPower ("repeatStrikesUpgrade", 7, 103.73E+3, 3, 600, 30, 0));
        heroPowersData.Add (new HeroPower ("critChanceUpgrade", 17, 1.43E+08, 3, 1800, 30, 0));
        heroPowersData.Add (new HeroPower ("sideHeroAttackRateUpgrade", 150, 1.98E+11, 50, 1800, 30, 0));
        heroPowersData.Add (new HeroPower ("tapDamageUpgrade", 70, 2.74E+14, 30, 1800, 30, 0));
        heroPowersData.Add (new HeroPower ("goldDropUpgrade", 15, 3.80E+17, 5, 1800, 30, 0));
    }

    public static void updateNextLevelData (int pos, int level)
    {
        float a = 0;
        float b = 0;
        switch (pos) {
        case 0:
            a = 0;
            b = 0;
            break;
        case 1:
            a = 0.0262f;
            b = 11.572f;
            break;
        case 2:
            a = 74.997f;
            b = 7.2321f;
            break;
        case 3:
            a = 5748f;
            b = 10.125f;
            break;
        case 4:
            a = 7E+07f;
            b = 7.9547f;
            break;
        case 5:
            a = 2E+10f;
            b = 9.402f;
            break;
        case 6:
            a = 3E+13f;
            b = 9.4012f;
            break;
        }
        if (a != 0) {
            heroPowersData [pos].m_Level = level + 1;
            heroPowersData [pos].m_Cost = System.Math.Floor (a * (System.Math.Exp (b * (float)heroPowersData [pos].m_Level)));
            if (level == 0) {

            } else {
                heroPowersData [pos].m_Increment += heroPowersData [pos].m_BaseIncrement;
            }

        } else {
            heroPowersData [pos].m_Level = level + 1;
            int x = heroPowersData [pos].m_Level;
            if (x >= 1 && x <= 20) {
                heroPowersData [pos].m_Cost = 5.43f * System.Math.Exp (0.1495f * (float)(x - 1));
            } else if (x >= 21) {
                heroPowersData [pos].m_Cost = 121f * System.Math.Exp (0.0714f * (float)(x - 20));
            }
            heroPowersData [pos].m_Increment = System.Math.Round ((0.05f * x + 1.05f) * (System.Math.Pow (1.05f, x)));
//            heroPowersData [pos].m_Cost = Mathf.Floor ((float)(0.0108f * Mathf.Pow (x, 3) - .3501f * Mathf.Pow (x, 2) + 8.2994 * x - 20.896));
//            heroPowersData [pos].m_Increment = Mathf.Floor (1.196f * (Mathf.Exp (0.0673f * (float)heroPowersData [pos].m_Level)));
        }

    }
}
