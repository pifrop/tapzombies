﻿using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour
{
    public static double basicTapDamage = 1;
    public static float criticalChance;
    public static float critMultiplierMin = 4;
    public static float critMultiplierMax = 10;
    public static bool isCrit = false;


    private static float GetCriticalMultiplier ()
    {
        return Random.Range (critMultiplierMin, critMultiplierMax);
    }

    public static double GetFinalTapDamage ()
    {
        // Get side hero damage as global static variable for calculations.
        double damage;

        if ((Random.Range (0, 100)) <= criticalChance) {
            damage = basicTapDamage * GetCriticalMultiplier ();
            isCrit = true;
        } else {
            damage = basicTapDamage;
            isCrit = false;
        }
        return damage;
    }

    public static void UpdateBasicTapDamage (double incr)
    {
        basicTapDamage += incr;
    }


}
