﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData
{
    public static GameData current;

    public Dictionary<AchievementsData.AchievementKey, AchievementsData.Achievement> achievementsSave;
//	public Dictionary<HeroPowersHandlerTZ.SkillType,float> SkillTimersSave;
    public List<HeroPowersData.HeroPower> heroPowersSave;
    public List<WeaponsData.Weapon> weaponsSave;

//	public PlayerData playerData;
    public double tapDamageSave;
    public float critChanceSave;
    public float critMultiplierMinSave;
    public float critMultiplierMaxSave;

//	public LevelDataUtil levelData;
    public int levelSave;
    public int noOfLevelEnemiesSave;
    public double enemyHealthSave;
    public double enemyLootDropSave;
    public int bossMultiplierSave;

    public int noOfEnemiesLeft;
    public double goldData;
    public double tapsData;
    public double diamondData;
    public bool isFarming;

    public int heroCurrentWeaponData;
    public List<WeaponsData.WeaponType> weaponTypeUnlockStatusSave;
}
