using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttributesData : MonoBehaviour
{
	public class Attribute
	{
		public string m_Name;
		public float m_Increment;
		public double m_Cost;
		public int m_Level;

		public Attribute (string name, float incr, double cost, int level)
		{
			m_Name = name;
			m_Increment = incr;
			m_Cost = cost;
			m_Level = level;
		}
	}

	public static List<Attribute> attributesData = new List<Attribute> ();

	void Start ()
	{
		attributesData.Add (new Attribute ("IncrTapDamage", 5, 0, 1));
		attributesData.Add (new Attribute ("IncrGoldDropped", 10, 0, 1));
		attributesData.Add (new Attribute ("IncrCritDamage", 10, 0, 1));
		attributesData.Add (new Attribute ("IncrTreasureChestGold", 10, 0, 1));
		attributesData.Add (new Attribute ("IncrAllDamage", 10, 0, 1));
		attributesData.Add (new Attribute ("IncrTapDamageByDps", 0.4f, 0, 1));
		attributesData.Add (new Attribute ("IncrCriticalChance", 1, 0, 1));
		attributesData.Add (new Attribute ("IncrDamageOnBoss", 5, 0, 1));
	}
}	
