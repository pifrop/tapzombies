﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponsData : MonoBehaviour
{
    public enum WeaponType
    {
        Cudgel,
        Axe,
        Sword,
        Mace,
        BattleHammer,
        Rare,
        Epic,
        Legendary
    }

    public class Weapon
    {
        public string m_Name;
        public WeaponType m_Type;
        public double m_Dps;
        public double m_WeaponDps = 0;
        public double m_Cost;
        public int m_Level;
        public double m_BaseCost;
        public double m_BaseDps;

        public Weapon (string name, WeaponType type, double dps, double cost, int level)
        {
            m_Name = name;
            m_Type = type;
            m_Dps = dps;
            m_Cost = cost;
            m_Level = level;
            m_BaseCost = cost;
            m_BaseDps = dps;
        }
    }

    private static List<double> weaponsDps = new List<double> (new double[] {4, 16, 56, 188, 807, 003.29E+3, 014.14E+3, 063.64E+3,
        440.12E+3, 001.74E+6, 007.15E+6, 030.65E+6, 136.87E+6,
        001.08E+9, 005.35E+9, 076.51E+9, 547.07E+9, 391.16E+9,
        419.52E+12, 009.00E+15, 212.72E+15, 015.21E+18, 002.17E+21,
        621.41E+21, 487.60E+24, 038.45E+30, 301.69E+39, 237.36E+54,
        018.70E+75, 013.37E+90, 001.14E+111, 090.11E+129, 007.10E+150});
    public static List<double> weaponsCost = new List<double> (new double[] {50, 175, 674, 2850, 1.33e+04, 6.810e+4, 3.840e+5,
        2.380e+6, 2.380e+7, 1.430e+8, 9.430e+8, 6.840e+9,
        5.470e+10, 8.200e+11, 8.200e+12, 1.640e+14, 1.640e+15,
        4.920e+16, 2.460e+18, 7.380e+19, 2.440e+21, 2.440e+23,
        4.870e+25, 1.950e+28, 2.140e+31, 2.360e+36, 2.590e+46,
        2.850e+61, 3.140e+81, 3.140e+96, 3.760e+116, 4.140e+136, 4.560e+156});
//    public static List<double> weaponsCost = new List<double> (new double[] {5, 7, 10, 13, 16, 19, 3.840e+5,
//        2.380e+6, 2.380e+7, 1.430e+8, 9.430e+8, 6.840e+9,
//        5.470e+10, 8.200e+11, 8.200e+12, 1.640e+14, 1.640e+15,
//        4.920e+16, 2.460e+18, 7.380e+19, 2.440e+21, 2.440e+23,
//        4.870e+25, 1.950e+28, 2.140e+31, 2.360e+36, 2.590e+46,
//        2.850e+61, 3.140e+81, 3.140e+96, 3.760e+116, 4.140e+136, 4.560e+156});
    public static List<Weapon> weaponData = new List<Weapon> ();
    
    void Awake ()
    {
       
    }

    public static void AddWeapons (string weapName, int weapPos)
    {
        WeaponType type = WeaponType.Cudgel;
        if (weapPos >= 0 && weapPos <= 2) {
            type = WeaponType.Cudgel;
        } else if (weapPos >= 3 && weapPos <= 5) {
            type = WeaponType.Axe;
        } else if (weapPos >= 6 && weapPos <= 8) {
            type = WeaponType.Sword;
        } else if (weapPos >= 9 && weapPos <= 11) {
            type = WeaponType.Mace;
        } else if (weapPos >= 12 && weapPos <= 13) {
            type = WeaponType.BattleHammer;
        } else if (weapPos >= 14 && weapPos <= 15) {
            type = WeaponType.Rare;
        } else if (weapPos >= 14 && weapPos <= 15) {
            type = WeaponType.Epic;
        } else {
            type = WeaponType.Legendary;
        }
        weaponData.Add (new Weapon (weapName, type, weaponsDps [weapPos], weaponsCost [weapPos], 0));
    }

    public static void updateNextLevelData (int pos, int level)
    {
        weaponData [pos].m_Level = level + 1;
        weaponData [pos].m_WeaponDps += weaponData [pos].m_Dps;
//        weaponData [pos].m_Cost = Mathf.Floor (weaponData [pos].m_BaseCost * (Mathf.Exp (0.0723f * (float)weaponData [pos].m_Level)));
        weaponData [pos].m_Cost = System.Math.Floor (weaponData [pos].m_BaseCost * System.Math.Pow (1.075f, (float)weaponData [pos].m_Level));
        weaponData [pos].m_Dps = System.Math.Floor (weaponData [pos].m_BaseDps * (System.Math.Exp (0.0437f * (float)weaponData [pos].m_Level)));
        // every percentage of dps increase, change base dps
    }
   
    
//    public static void UpdateBasicWeaponDamage (double incr)
//    {
//        GameManagerTZ.weaponDps += incr;
//    }
}
