﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AchievementsData : MonoBehaviour
{
    public enum AchievementKey
    {
        MonstersKilled=0,
        TotalGoldCollected=1,
        LevelReached=2,
        DpsReached=3,
        BossesKilled=4,
        Hits=5,
        WeaponUpgrades=6,
        CriticalHits=7,
        Charges=8
    }

    // Achievements
    public static Achievement noOfMonstersKilled = new Achievement ("MonstersKilled", 0, 100, 1E+3, 1E+4, 1E+40, 1E+40, 1E+80);
    public static Achievement totalGoldCollected = new Achievement ("GoldCollected", 0, 100E+3, 1E+15, 1E+24, 1E+40, 1E+40, 1E+80);
    public static Achievement levelreached = new Achievement ("LevelReached", 0, 15, 75, 375, 1E+40, 1E+40, 1E+80);
    public static Achievement relicsCollected = new Achievement ("RelicsCollected", 0, 5, 15, 1E+4, 1E+40, 1E+40, 1E+80);
    public static Achievement artifactsOwned = new Achievement ("ArtifactsOwned", 0, 2, 10, 1E+4, 1E+40, 1E+40, 1E+80);
    public static Achievement totalDPSReached = new Achievement ("DpsReached", 0, 1E+3, 1E+10, 1E+25, 1E+40, 1E+40, 1E+80);
    public static Achievement noOfBossesKilled = new Achievement ("BossesKilled", 0, 10, 100, 1E+3, 1E+40, 1E+40, 1E+80);
    public static Achievement noOfHits = new Achievement ("HitsReached", 0, 1E+3, 1E+5, 1E+14, 1E+40, 1E+40, 1E+80);
    public static Achievement noOfPrestiges = new Achievement ("PrestigesDone", 0, 1, 1E+3, 1E+4, 1E+40, 1E+40, 1E+80);
    public static Achievement noOfWeaponUpgrades = new Achievement ("WeaponUpgradesAchieved", 0, 200, 2E+3, 1E+4, 1E+40, 1E+40, 1E+80);
    public static Achievement noOfChestsOpened = new Achievement ("ChestsOpened", 0, 10, 100, 1E+4, 1E+40, 1E+40, 1E+80);
    public static Achievement noOfFairyPresents = new Achievement ("FairyPresentsOpened", 0, 5, 50, 500, 1E+40, 1E+40, 1E+80);
    public static Achievement noOfCriticalHIts = new Achievement ("CriticalHits", 0, 10, 5E+3, 1E+4, 1E+40, 1E+40, 1E+80);
    public static Achievement noOfCharges = new Achievement ("Charges", 0, 3, 30, 1E+4, 1E+40, 1E+40, 1E+80);
	
    public class Achievement
    {
        public string m_Name;
        public double m_Value;
        private int m_Diamonds;
        public int m_Stars;
        public List<double> m_StarConstants = new List<double> ();
        //		private double m_Star1Constant;
        //		private double m_Star2Constant;
        //		private double m_Star3Constant;
        //		private double m_Star4Constant;
        //		private double m_Star5Constant;
		
        public  List<double> Diamonds = new List<double> (new double[] {15,25,50,75,100,200,400 });
        public Achievement (string name, double value, double s1Constant, double s2Constant, double s3Constant, double s4Constant, double s5Constant, double s6Constant)
        {
            m_Name = name;
            m_Value = value;
            m_Stars = 0;
            m_StarConstants.Add (s1Constant);
            m_StarConstants.Add (s2Constant);
            m_StarConstants.Add (s3Constant);
            m_StarConstants.Add (s4Constant);
            m_StarConstants.Add (s5Constant);
            m_StarConstants.Add (s6Constant);
			
        }
		
        public bool isUnlocked ()
        {
            if (m_Value >= m_StarConstants [m_Stars]) {
                return true;
            } else {
                return false;
            }
        }
		
        public void IncrStarsListPos ()
        {
            m_Stars += 1;
            Diamonds [m_Stars].Equals (Diamonds [m_Stars + 1]);
			
        }
		
		
    }
    public static List<Achievement> achievements = new List<Achievement> ();
    public static Dictionary <AchievementKey, Achievement> achievementsDictionary = new Dictionary<AchievementKey, Achievement> ();
	
    void Awake ()
    {
        achievementsDictionary.Add (AchievementKey.MonstersKilled, noOfMonstersKilled);
        achievementsDictionary.Add (AchievementKey.TotalGoldCollected, totalGoldCollected);
        achievementsDictionary.Add (AchievementKey.LevelReached, levelreached);
        achievementsDictionary.Add (AchievementKey.DpsReached, totalDPSReached);
        achievementsDictionary.Add (AchievementKey.BossesKilled, noOfBossesKilled);
        achievementsDictionary.Add (AchievementKey.Hits, noOfHits);
        achievementsDictionary.Add (AchievementKey.WeaponUpgrades, noOfWeaponUpgrades);
        achievementsDictionary.Add (AchievementKey.CriticalHits, noOfCriticalHIts);
        achievementsDictionary.Add (AchievementKey.Charges, noOfCharges);
    }

    void Start ()
    {
//        achievements.Add (noOfMonstersKilled);
//        achievements.Add (totalGoldCollected);
//        achievements.Add (levelreached);
////        achievements.Add (relicsCollected);
////        achievements.Add (artifactsOwned);
//        achievements.Add (totalDPSReached);
//        achievements.Add (noOfBossesKilled);
//        achievements.Add (noOfTaps);
////        achievements.Add (noOfPrestiges);
//        achievements.Add (noOfWeaponUpgrades);
////        achievements.Add (noOfChestsOpened);
////        achievements.Add (noOfFairyPresents);
//        achievements.Add (noOfCriticalHIts);
//        achievements.Add (noOfCharges);
		
    }
}