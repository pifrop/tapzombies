using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using Soomla.Store;

public class DiamondCrystalsData : MonoBehaviour
//public class DiamondCrystalsData : IStoreAssets
{
//    public int GetVersion ()
//    {
//        return 0;
//    }
//
//    public VirtualCurrency[] GetCurrencies ()
//    {
//        return new VirtualCurrency[]{DIAMOND_CURRENCY};
//    }
//
//    public VirtualGood[] GetGoods ()
//    {
//        return new VirtualGood[] {DIAMONDCAKE_GOOD};
//    }
//
//    public VirtualCurrencyPack[] GetCurrencyPacks ()
//    {
//        return new VirtualCurrencyPack[] {
//            DIAM170_PACK,
//            DIAM500_PACK,
//            DIAM1200_PACK,
//            DIAM3100_PACK,
//            DIAM6500_PACK,
//            DIAM14000_PACK
//        };
//    }
//
//    public VirtualCategory[] GetCategories ()
//    {
//        return new VirtualCategory[]{GENERAL_CATEGORY};
//    }
//
//    /** Static Final Members **/
//    public const string DIAMOND_CURRENCY_ITEM_ID = "currency_diamond";
//
//    public const string DIAM170_PACK_PRODUCT_ID = "com.pifrop.170diam";
//    
//    public const string DIAM500_PACK_PRODUCT_ID = "com.pifrop.500diam";
//    
//    public const string DIAM1200_PACK_PRODUCT_ID = "com.pifrop.1200diam";
//    
//    public const string DIAM3100_PACK_PRODUCT_ID = "com.pifrop.3100diam";
//
//    public const string DIAM6500_PACK_PRODUCT_ID = "com.pifrop.6500diam";
//
//    public const string DIAM14000_PACK_PRODUCT_ID = "com.pifrop.14000diam";
//
//    public static VirtualCurrency DIAMOND_CURRENCY = new VirtualCurrency (
//        "DIAMONDs",                                      // name
//        "",                                             // description
//        DIAMOND_CURRENCY_ITEM_ID                         // item id
//    );
//
//    /** Virtual Currency Packs **/
//    
//    public static VirtualCurrencyPack DIAM170_PACK = new VirtualCurrencyPack (
//        "170 diamonds",                                   // name
//        "Test refund of an item",                       // description
//        DIAM170_PACK_PRODUCT_ID,                                   // item id
//        170,                                             // number of currencies in the pack
//        DIAMOND_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket (DIAM170_PACK_PRODUCT_ID, 50)
//    );
//    
//    public static VirtualCurrencyPack DIAM500_PACK = new VirtualCurrencyPack (
//        "50 DIAMONDs",                                   // name
//        "Test cancellation of an item",                 // description
//        "DIAMONDs_50",                                   // item id
//        500,                                             // number of currencies in the pack
//        DIAMOND_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket (DIAM500_PACK_PRODUCT_ID, 100)
//    );
//    
//    public static VirtualCurrencyPack DIAM1200_PACK = new VirtualCurrencyPack (
//        "400 DIAMONDs",                                  // name
//        "Test purchase of an item",                     // description
//        "DIAMONDs_400",                                  // item id
//        1200,                                            // number of currencies in the pack
//        DIAMOND_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket (DIAM1200_PACK_PRODUCT_ID, 200)
//    );
//    
//    public static VirtualCurrencyPack DIAM3100_PACK = new VirtualCurrencyPack (
//        "1000 DIAMONDs",                                 // name
//        "Test item unavailable",                        // description
//        "DIAMONDs_1000",                                 // item id
//        3100,                                           // number of currencies in the pack
//        DIAMOND_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket (DIAM3100_PACK_PRODUCT_ID, 400)
//    );
//
//    public static VirtualCurrencyPack DIAM6500_PACK = new VirtualCurrencyPack (
//        "1000 DIAMONDs",                                 // name
//        "Test item unavailable",                        // description
//        "DIAMONDs_1000",                                 // item id
//        6500,                                           // number of currencies in the pack
//        DIAMOND_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket (DIAM6500_PACK_PRODUCT_ID, 800)
//    );
//
//    public static VirtualCurrencyPack DIAM14000_PACK = new VirtualCurrencyPack (
//        "1000 DIAMONDs",                                 // name
//        "Test item unavailable",                        // description
//        "DIAMONDs_1000",                                 // item id
//        14000,                                           // number of currencies in the pack
//        DIAMOND_CURRENCY_ITEM_ID,                        // the currency associated with this pack
//        new PurchaseWithMarket (DIAM14000_PACK_PRODUCT_ID, 1600)
//    );
//
//    public static VirtualGood DIAMONDCAKE_GOOD = new SingleUseVG (
//        "Fruit Cake",                                               // name
//        "Customers buy a double portion on each purchase of this cake", // description
//        "fruit_cake",                                               // item id
//        new PurchaseWithVirtualItem (DIAMOND_CURRENCY_ITEM_ID, 225));
//
//    /** Virtual Categories **/
//    // The DIAMOND rush theme doesn't support categories, so we just put everything under a general category.
//    public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory (
//        "General", new List<string> (new string[] {
//    })
//    );
//    
}

