﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyPropsTZ : MonoBehaviour
{
    
    public enum EnemyType
    {
        Normal,
        Boss
    }
    
    public enum EnemyState
    {
        Idle,
        Hit,
        Dying
    }
    
    public enum SlashDirection
    {
        RightToLeft,
        LeftToRight,
        TopToBottom,
        BottomToTop,
        Unknown
    }
    
    public EnemyType enemyType = EnemyType.Normal;

    // Health and Armour
    public double health = 100;
    public double maxHealth = 100;
//	public SpriteRenderer healthBar;
    public float armour = 0;
    private Vector3 healthScale;
    private float timerToKill = 0.0f;

    
    // Movement
    public EnemyState state = EnemyState.Idle;
//	private float leftBorder, rightBorder, topBorder;
    private float speed;

    // Attack Damage
    public float baseAttackDamage = 0.0f;
    public float attackRate = 0.0f;
    public float critPercent = 0;
    
    // Hit Damage
    private Vector3 slashStart;
    private Vector3 slashEnd;
    private Vector2 hitForce;
    private float lastHitTime;
    private float stunTime;
    private SlashDirection hitDir;
    
    // AI and animation
    private SpriteRenderer enemySprite;
    protected Animator animator;
    public GameObject hitEffect;
    public GameObject deathEffect;
    public AudioClip hitSound;
    public AudioClip deathSound;

    // Reward and combos
    public double normCurrDrop;
    
    //    Vector3 prevPos;
    
    private EnemyAnimsTZ enemyAnims;
    public Animator enemyAnimator;
    // Test
    private Color enemyColor;
    private Quaternion initialRotation;
    private Vector3 initialPosition;

    void Awake ()
    {
//		healthScale = healthBar.transform.localScale;
    }
    void Start ()
    {
//		health = maxHealth;
//		GameUIManagerTZ.UpdateHealthDisplay (LevelDataUtil.ToStringFormat (health));
//		GameUIManagerTZ.UpdateHealthBar (100);
//		gameObject.transform.position = Vector3.zero;
//		PrepareEnemy ();

        enemyAnims = GetComponent <EnemyAnimsTZ> ();
        this.gameObject.tag = "Enemy";
        // Test 
//        InvokeRepeating ("OnTestHit", 0.5f, 0.5f);
    }

    void Update ()
    {
        CheckHealth ();
        transform.rotation = initialRotation;
        transform.position = initialPosition;
    }
    
    private void SetMovementLimiters ()
    {
//		float topYPos = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 10)).y;
//		float bottomYPos = Camera.main.ScreenToWorldPoint (new Vector3 (0, 0, 10)).y;
//		topBorder = bottomYPos + ((Mathf.Abs (topYPos) + Mathf.Abs (bottomYPos)) * (0.821f));
    }
    
    IEnumerator OnSpawn ()
    {   
        if (!PoolManager.Props.ContainsKey (transform.parent.name))
            PoolManager.Props.Add (transform.parent.name, this);
//		PrepareEnemy ();
//        gameObject.transform.RotateAround (Vector3.zero, Vector3.up, 180);
        GameUIManagerTZ.UpdateEnemyName (transform.name);
        initialRotation = transform.rotation;
        initialPosition = transform.position;
        yield return new WaitForEndOfFrame ();
    }
    
    public void PrepareEnemy ()
    {
        health = maxHealth;
        GameUIManagerTZ.UpdateHealthDisplay (LevelDataUtil.ToStringFormat (health));
        GameUIManagerTZ.UpdateHealthBar (100);
        state = EnemyState.Idle; // Set enemy Default state.
        animator = GetComponent<Animator> ();
        if (enemyType == EnemyType.Boss) {
            transform.localScale += new Vector3 (0.4F, 0.4F, 0.4F);
            timerToKill = Time.time + 30.0f;
        }
    }

    void CheckHealth ()
    {

        health = Mathf.Clamp ((float)health, -0.0005f, (float)maxHealth);
        float healthReduction = Mathf.Round ((float)(health / maxHealth) * 100.0f);
//		if (healthBar) {
//			healthBar.sharedMaterial.color = Color.Lerp (Color.green, Color.red, 1 - healthReduction * 0.01f);
//			healthBar.transform.localScale = new Vector3 (healthScale.x * healthReduction * 0.01f, healthScale.y, healthScale.z);
//		}
        if (enemyType == EnemyType.Boss) {
            float bossTimer = timerToKill - Time.time;
            if (bossTimer <= 0)
                bossTimer = 0;
            if ((bossTimer <= 0) && (health > 0) && state != EnemyState.Dying) {
                OnFailedToKill ();
            }
            GameUIManagerTZ.UpdateBossTimer ((bossTimer).ToString ("f1"));
        } else {
            GameUIManagerTZ.UpdateBossTimer (string.Empty);
        }

        if (health <= 0 && state != EnemyState.Dying) {
            OnDeath ();
        } else {
            GameUIManagerTZ.UpdateHealthBar (healthReduction);
        }
    }

    public void Hit (double slashDamage, Vector2 hForce, Vector3 vecStart, Vector3 vecEnd)
    {
        slashStart = vecEnd;
        slashEnd = vecStart;
        hitForce = hForce;
        EnemyHit (slashDamage, true);
    }
    
    public void sideHeroHit (double sHeroDamage)
    {
        EnemyHit (sHeroDamage, false);
    }

    public Vector3 StatusEffect ()
    {
        return transform.position;
    }

    void EnemyHit (double damage, bool isTap)
    {   
        if (isTap) {
            if ((Time.time - lastHitTime) > 0.3) {
                if (hitEffect)
                    PoolManager.Pools ["ParticleEffects"].Spawn (hitEffect, slashEnd, Quaternion.identity);
                lastHitTime = Time.time;
                Vector3 popUpPos = new Vector3 (transform.position.x, transform.position.y + 1.3f, transform.position.z - 0.5f);
                PopDamageScript popupScript = GameUIManagerTZ.CreatePopupString (popUpPos);
                popupScript.AttachString (LevelDataUtil.ToStringFormat (damage));
                popupScript.destructionTime = 2.0f;
                popupScript.risingSpeed = 0.8f;
                if (PlayerData.isCrit) {
                    popupScript.displayText.fontSize += 4;
//                    popupScript.displayText.color = new Color32 (100, 100, 100, 255);
                    GameManagerTZ.HitInfo (damage, true);
                } else {
                    GameManagerTZ.HitInfo (damage, false);
                }
                OnHit (damage);
                HitAnimation ();
            }
        } else {
            OnHit (damage);
        }
    }

    //play hit sound and instantiate hit particle effect
    void OnHit (double damage)
    {

        health -= damage;
//        Rigidbody rigidBody = gameObject.GetComponent <Rigidbody> ();
//        rigidBody.AddForceAtPosition (new Vector3 (0, 0, -1));
        GameUIManagerTZ.UpdateHealthDisplay (LevelDataUtil.ToStringFormat (health));

//		StartCoroutine ("HitAnimation");

//        if (hitEffect)
//            PoolManager.Pools ["ParticleEffects"].Spawn (hitEffect, transform.position, hitEffect.transform.rotation);
    }

    void HitAnimation ()
    {
//		if (_crossFade) {	
//			if (_lastAnim == (_animations [i + page * maxButtons]))
//				this._animator.Play ("");
//			_animator.CrossFade (_animations [i + page * maxButtons], .1);
//			this._lastAnim = _animations [i + page * maxButtons];
//		} else {
        enemyAnims.Blink (1, 0.1f);
        int animPos = Random.Range (1, 3);
        if (enemyAnimator)
            enemyAnimator.SetTrigger ("Hurt" + animPos.ToString ());
//		AnimatorStateInfo stateInfo = GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0);
//		yield return new WaitForSeconds (stateInfo.length);
//		GetComponent<Animator> ().SetBool ("IsHit", false);
//		}
    }

    public void InstaKillEnemy ()
    {
        OnDeath ();
    }

    void OnDeath ()
    {
        state = EnemyState.Dying;
//        enemyAnims.Decapitate (false, 1, Vector3.zero);
        GameManagerTZ.DropFinalEnemyMoney (normCurrDrop, (enemyType == EnemyType.Boss));
        if (enemyAnimator)
            enemyAnimator.SetTrigger ("Hurt3");
//		StartCoroutine ("RemoveEnemy");
        Invoke ("RemoveEnemy", 1.2f);
        Invoke ("SpawnAnother", 1.7f);
    }

    void OnFailedToKill ()
    {
        Invoke ("RemoveEnemy", 2.0f);
        Invoke ("SpawnFarmingEnemies", 2.2f);
        state = EnemyState.Dying;
    }

    void SpawnFarmingEnemies ()
    {
        GameManagerTZ.FailedToKillBoss ();
    }

    void SpawnAnother ()
    {
        GameManagerTZ.EnemyKilled (enemyType == EnemyType.Boss);

    }
    
    public void RemoveEnemy ()
    {
//        if (health <= 0) {
//            animator.SetTrigger ("isDeath");
//            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo (0);
//            yield return new WaitForSeconds (stateInfo.length);
//        }
        //reset all initialized variables for later reuse
        if (deathEffect)
            PoolManager.Pools ["ParticleEffects"].Spawn (deathEffect, transform.position, Quaternion.identity);
        state = EnemyState.Idle;
//		health = maxHealth;
//		float healthReduction = 100.0f;
//		if (healthBar) {
//			healthBar.material.color = Color.Lerp (Color.green, Color.red, 1 - healthReduction * 0.01f);
//			healthBar.transform.localScale = new Vector3 (healthScale.x * healthReduction * 0.01f, healthScale.y, healthScale.z);
//		}
//        enemyAnims.ResetModel ();
        if (enemyType == EnemyType.Boss) {
            transform.localScale -= new Vector3 (0.4f, 0.4f, 0.4f);
        }
        gameObject.transform.parent.transform.rotation = Quaternion.identity;
        PoolManager.Pools ["Enemies"].Despawn (transform.parent.gameObject);
//		yield return new WaitForEndOfFrame ();
    }
    
}
