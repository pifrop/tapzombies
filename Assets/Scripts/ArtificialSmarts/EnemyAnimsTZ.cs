﻿using UnityEngine;
using System.Collections;

public class EnemyAnimsTZ : MonoBehaviour
{

	private Component[] boneRig;
	private float mass = 0.1f;
	public Transform projector;
	public Transform root;
	public Color bloodColor;
	public GameObject model;
	public Mesh bodyMesh;
	public Mesh fullBodyMesh;

	public ParticleSystem explodeHeadPS;
	public GameObject head;
	public Transform headBone;

	public GameObject[] disableWhenDecapitated;
	public ParticleSystem bodyPS;

	private bool decapitated;

	//Blinking
	private Color colorOriginal;
	private Color color;
	private float R = 2500;
	private float G = 2500;
	private float B = 2500;
	
	private bool randomColor;
	private int blinkCounter;
	private int stopBlink;

	void LastUpdate ()
	{
		if ((!GetComponent <Collider> ().enabled) && projector && root) {
			projector.transform.position = new Vector3 (root.position.x, projector.transform.position.y, root.position.z);
		}
	}

	void Start ()
	{
		if (!root)
			root = transform.FindChild ("Root");
		if (!projector)
			projector = transform.FindChild ("Blob Shadow Projector");
		if (!model)
			model = transform.FindChild ("MicroMale").gameObject;
		if (!headBone)
			headBone = transform.FindChild ("Head");
		boneRig = gameObject.GetComponentsInChildren <Rigidbody> (); 
		disableRagdoll ();
		//Blinking
		colorOriginal = model.GetComponent <Renderer> ().material.color;
	}

	public void Blink (int times, float speed, float red, float green, float blue)
	{
		CancelInvoke ();
		randomColor = false;
		R = red;
		G = green;
		B = blue;
		stopBlink = times;
		InvokeRepeating ("BlinkInvoke", speed, speed);
	}

	public void Blink (int times, float speed)
	{
		CancelInvoke ();
		randomColor = true;
		stopBlink = times;
		InvokeRepeating ("BlinkInvoke", speed, speed);
	}

	public void BlinkInvoke ()
	{
		if (blinkCounter < stopBlink) {
			if (randomColor) {
				color = new Color (Random.Range (1, 5), Random.Range (1, 5), Random.Range (1, 5), 1);
			} else {
				color = new Color (R, G, B, 1);
			}
			
			if (model.GetComponent<Renderer> ().material.color == colorOriginal) {
				model.GetComponent<Renderer> ().material.color = color;
			} else {
				model.GetComponent<Renderer> ().material.color = colorOriginal;
			}
			blinkCounter++;
		} else {
			model.GetComponent<Renderer> ().material.color = colorOriginal;
			blinkCounter = 0;
			CancelInvoke ();
		}
	}

	public void disableRagdoll ()
	{
		foreach (Rigidbody ragdoll in boneRig) {
			if (ragdoll.GetComponent<Collider> () && ragdoll.GetComponent<Collider> () != this.GetComponent<Collider> ()) {
				ragdoll.GetComponent<Collider> ().enabled = false;
				ragdoll.isKinematic = true;
				ragdoll.mass = 0.01f;
			}
		}
		GetComponent <Collider> ().enabled = true;
	}

	public IEnumerator enableRagdoll (float delay, Vector3 force)
	{
		yield return new WaitForSeconds (delay);
		foreach (Rigidbody ragdoll  in boneRig) {
			if (ragdoll.GetComponent <Collider> ())
				ragdoll.GetComponent <Collider> ().enabled = true;
			ragdoll.isKinematic = false; 
			ragdoll.mass = mass;
			if (force.magnitude > 0)
				ragdoll.AddForce (force * Random.value);
		}
		GetComponent<Animator> ().enabled = false;
		GetComponent<Collider> ().enabled = false;
		Destroy (GetComponent ("BotControlScript"));
		GetComponent<Rigidbody> ().isKinematic = true;
		GetComponent<Rigidbody> ().useGravity = false;
		for (int i =0; i < this.disableWhenDecapitated.Length; i++) {
			disableWhenDecapitated [i].SetActive (false);
		}
	}

	public void Decapitate (bool explode, float delay, Vector3 force)
	{
		if (!decapitated) {
			decapitated = true;
			model.GetComponent <SkinnedMeshRenderer> ().sharedMesh = this.bodyMesh;
			if (head) {
				if (!explode) {
					GameObject h = (GameObject)Instantiate (head, headBone.position, transform.rotation);
					h.transform.localScale = headBone.localScale * transform.localScale.x;
					Physics.IgnoreCollision (gameObject.GetComponent<Collider> (), h.GetComponent<Collider> ());
					Destroy (headBone.GetComponent<Collider> ());
					h.GetComponent<Renderer> ().sharedMaterial = model.GetComponent<SkinnedMeshRenderer> ().sharedMaterial;
					if (force.magnitude > 0)
						h.GetComponent<Rigidbody> ().AddForce (force * Random.value);
					h.GetComponent<Rigidbody> ().AddTorque (new Vector3 (Random.Range (-10, 10), Random.Range (-10, 10), Random.Range (-10, 10)));
					h.transform.FindChild ("Head PS").GetComponent<ParticleSystem> ().startColor = this.bloodColor;
					EnableCollisions (gameObject.GetComponent<Collider> (), h.GetComponent<Collider> ());
				} else {
					GameObject e = (GameObject)Instantiate (explodeHeadPS.gameObject, headBone.position, transform.rotation);
					e.GetComponent<ParticleSystem> ().startColor = this.bloodColor;
					Destroy (e, 2);
				}
				if (bodyPS) {
					bodyPS.startColor = this.bloodColor;
					bodyPS.Play ();
				}
				
			}
			enableRagdoll (delay, force);
		}
	}

	public void ResetModel ()
	{
		model.GetComponent <SkinnedMeshRenderer> ().sharedMesh = this.fullBodyMesh;
		decapitated = false;
	}

	public IEnumerator EnableCollisions (Collider c1, Collider c2)
	{
		yield return new WaitForSeconds (1);
		if (c2 && c1.enabled)
			Physics.IgnoreCollision (c1, c2, false);
	}
}
