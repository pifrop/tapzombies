﻿using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour
{

	public Animator animator;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}


	public void TestButton ()
	{
		if (animator.GetBool ("IsTestHit")) {
			animator.SetBool ("IsTestHit", false);
		} else {
			animator.SetBool ("IsTestHit", true);
		}
	}
}
