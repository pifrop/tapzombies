﻿using UnityEngine;
using System.Collections;

public class SampleAI : MonoBehaviour {

    public float moveSpeed = 2.0f;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate (Vector3.up * Time.deltaTime * moveSpeed);
	}
}
