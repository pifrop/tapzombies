//using UnityEngine;
//using System.Collections;
//
//public class SimpleHealthScript : MonoBehaviour {
//    // Inspector Variables
//    public float bar_health;
//    public int bar_maxHealth;
//    
//    // Private variables
//    private Rect health_box;
//    private Texture2D background;
//    private Texture2D foreground;
//    
//	// Use this for initialization
//    public SimpleHealthScript () {
//        
//    }
//	void Start () {
//	}
//    
//    public SimpleHealthScript HealthIntitalization (Rect h_box, float health, int maxHealth) {
//        var thisObj = GameObject.AddComponent(typeof(SimpleHealthScript));
//        thisObj.health_box = h_box;
//        thisObj.bar_health = health;
//        thisObj.bar_maxHealth = maxHealth;
//        
//        background = new Texture2D(1, 1, TextureFormat.RGB24, false);
//        foreground = new Texture2D(1, 1, TextureFormat.RGB24, false);
// 
//        background.SetPixel(0, 0, Color.red);
//        foreground.SetPixel(0, 0, Color.green);
// 
//        background.Apply();
//        foreground.Apply();
//        
//        return this;
//    }
//    
//    public void HealthGUIUpdate () {
//        GUI.BeginGroup(health_box);
//        {
//            GUI.DrawTexture(new Rect(0, 0, health_box.width, health_box.height), background, ScaleMode.StretchToFill);
//            GUI.DrawTexture(new Rect(0, 0, health_box.width*bar_health/bar_maxHealth, health_box.height), foreground, ScaleMode.StretchToFill);
//        }
//        GUI.EndGroup();
//    }
//   	
//	// Update is called once per frame
//	void Update () {
//	}
//}
