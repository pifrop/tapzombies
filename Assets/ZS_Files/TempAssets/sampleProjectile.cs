﻿using UnityEngine;
using System.Collections;

public class sampleProjectile : MonoBehaviour {
	float i = 0.2f;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("SizeUp", 1.0f, 0.1f);
		Debug.Log (Mathf.Cos(45f));
	}
	
	// Update is called once per frame
	void Update () {
		if (i >= 3.0f)
			CancelInvoke ("SizeUp");
	}

	void SizeUp () {
		transform.localScale = new Vector3(i, i, i);
		transform.Translate (i*0.081f, i*0.081f, 0);
		i += 0.2f;
	}
}
